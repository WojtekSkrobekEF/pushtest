import { OutcomeMessage } from '../../src/domains/OutcomeMessage';
import Optional from 'typescript-optional';
import { messageDao } from '../../src/dao/MessageDAO';
import chai from 'chai';
import { equal } from 'should';
import { DATABASE_SCHEMA, INBOUND_MESSAGE_TABLE, OUTBOUND_MESSAGE__TABLE } from '../../src/configs/env_config';
import { IncomeMessage } from '../../src/domains/IncomeMessage';
import { MessageType } from '../../src/enums/MessageType';
import { OutcomeSMSMessageDTO } from '../../src/dto/OutcomeSMSMessageDTO';
import { PhoneNumbers } from '../../src/enums/PhoneNumbers';

const TEST_INSERT_QUERY_OUTCOME_MESSAGE_1: string =
	'INSERT INTO ' +
	DATABASE_SCHEMA +
	OUTBOUND_MESSAGE__TABLE +
	'(fromNumber__c, toNumber__c, message__c, messageSid__c,' +
	' parent_id__c, status__c, sobject__c, createddate , opportunity__c, isdeleted, type__c)' +
	" VALUES('+14777888','+37544888777','Hola amigo','sid123'" +
	",'parentid123','sent', 'Opportunity', '2018-10-08 17:27:26' , 'parentid123', false, 'SMS')";

const TEST_INSERT_QUERY_OUTCOME_MESSAGE_2: string =
	'INSERT INTO ' +
	DATABASE_SCHEMA +
	OUTBOUND_MESSAGE__TABLE +
	'(fromNumber__c, toNumber__c, message__c, messageSid__c, parent_id__c,' +
	' status__c, sobject__c, createddate, isdeleted, type__c)' +
	" VALUES('+14777888','+37544888777','Amigo, como estas?','sid124'," +
	"'parentid124','delivered' ,'test', '2018-10-08 17:00:26', false, 'SMS')";

const TEST_INSERT_QUERY_OUTCOME_MESSAGE_3: string =
	'INSERT INTO ' +
	DATABASE_SCHEMA +
	OUTBOUND_MESSAGE__TABLE +
	'(fromNumber__c, toNumber__c, message__c, messageSid__c,' +
	' parent_id__c, status__c, sobject__c, createddate , opportunity__c, isdeleted, type__c)' +
	" VALUES('+14777888','+37544888777','Hola amigo, are you here? ','sid125'" +
	",'parentid125','delivered', 'Opportunity', '2018-10-08 17:30:00' , 'parentid125', false, 'SMS')";

const TEST_INSERT_QUERY_INCOME_MESSAGE_1: string =
	'INSERT INTO ' +
	DATABASE_SCHEMA +
	INBOUND_MESSAGE_TABLE +
	'(fromNumber__c, toNumber__c, message__c, messageSid__c, status__c, isdeleted, type__c)' +
	" VALUES('+37544888777','+14777888','Buenos dias!','sid125','delivered', false, 'SMS')";

const TEST_INSERT_QUERY_INCOME_MESSAGE_2: string =
	'INSERT INTO ' +
	DATABASE_SCHEMA +
	INBOUND_MESSAGE_TABLE +
	'(fromNumber__c, toNumber__c, message__c, messageSid__c, status__c, isdeleted, type__c)' +
	" VALUES('+37544888777','+14777888','Esta bien! Como tu estas?','sid126','delivered', false, 'SMS')";

const CLEAR_INCOME_MESSAGE: string =
	'DELETE FROM ' +
	DATABASE_SCHEMA +
	INBOUND_MESSAGE_TABLE +
	" where messageSid__c in ('sid124','sid125','msqn123','sid126','msqn124','msqn127')";

const CLEAR_OUTCOME_MESSAGE: string =
	'DELETE FROM ' +
	DATABASE_SCHEMA +
	OUTBOUND_MESSAGE__TABLE +
	" where messageSid__c in ('sid123', 'sid124','sid125','msgid123')";

const SELECT_INBOUND_MESSAGE: string =
	'SELECT * FROM ' + DATABASE_SCHEMA + INBOUND_MESSAGE_TABLE + ' where messageSid__c = $1';

const SELECT_OUTBOUND_MESSAGE: string =
	'SELECT * FROM ' + DATABASE_SCHEMA + OUTBOUND_MESSAGE__TABLE + ' where messageSid__c=$1';

const TWILIO_NUMBER = '+14702648740';

beforeEach('Inserting data into DB', async function () {
	console.log('INSERTING TEST DATA');

	await messageDao.getDb().none(CLEAR_INCOME_MESSAGE);
	await messageDao.getDb().none(CLEAR_OUTCOME_MESSAGE);
	await messageDao.getDb().none(TEST_INSERT_QUERY_OUTCOME_MESSAGE_1);
	await messageDao.getDb().none(TEST_INSERT_QUERY_OUTCOME_MESSAGE_2);
	await messageDao.getDb().none(TEST_INSERT_QUERY_OUTCOME_MESSAGE_3);
	await messageDao.getDb().none(TEST_INSERT_QUERY_INCOME_MESSAGE_1);
	await messageDao.getDb().none(TEST_INSERT_QUERY_INCOME_MESSAGE_2);
});

afterEach('Remove test data', async function () {
	console.log('CLEARING DATABASE');

	await messageDao.getDb().none(CLEAR_INCOME_MESSAGE);
	await messageDao.getDb().none(CLEAR_OUTCOME_MESSAGE);
});

describe('Test Message DAO save outcome message', function () {
	it('Testing method for saving outcome message', async () => {
		const messageSid = 'msgid123';
		const message: OutcomeMessage = new OutcomeMessage(
			Optional.empty(),
			'445566',
			TWILIO_NUMBER,
			'Test',
			'0063264278',
			'ABC123',
			'Opportunity',
			MessageType.SMS,
		);

		const dbRecord = await messageDao.saveOutcomeMessage(message);

		await messageDao.updateMessageSidById(messageSid, dbRecord.id);

		const res: Record<string, unknown>[] = await messageDao.getDb().any(SELECT_OUTBOUND_MESSAGE, messageSid);

		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('message__c', 'Test');
		chai.expect(outboundMsg).to.have.property('fromnumber__c', '445566');
		chai.expect(outboundMsg).to.have.property('tonumber__c', TWILIO_NUMBER);
		chai.expect(outboundMsg).to.have.property('parent_id__c', '0063264278');
		chai.expect(outboundMsg).to.have.property('sobject__c', 'Opportunity');
		chai.expect(outboundMsg).to.have.property('opportunity__c', '0063264278');
		chai.expect(outboundMsg).to.have.property('type__c', MessageType.SMS);
	});
});

describe('Test Message DAO save outcome message', function () {
	it('Outcome message has not  sObject opportunity', async () => {
		const messageSid = 'msgid123';
		const message: OutcomeMessage = new OutcomeMessage(
			Optional.empty(),
			'445566',
			TWILIO_NUMBER,
			'Test',
			'0063264278',
			'ABC123',
			'test',
			MessageType.SMS,
		);

		const dbRecord = await messageDao.saveOutcomeMessage(message);

		await messageDao.updateMessageSidById(messageSid, dbRecord.id);

		const res: Record<string, unknown>[] = await messageDao.getDb().any(SELECT_OUTBOUND_MESSAGE, messageSid);
		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('message__c', 'Test');
		chai.expect(outboundMsg).to.have.property('fromnumber__c', '445566');
		chai.expect(outboundMsg).to.have.property('tonumber__c', TWILIO_NUMBER);
		chai.expect(outboundMsg).to.have.property('parent_id__c', '0063264278');
		chai.expect(outboundMsg).to.have.property('sobject__c', 'test');
		chai.expect(outboundMsg).to.have.property('opportunity__c', null);
		chai.expect(outboundMsg).to.have.property('type__c', MessageType.SMS);
	});
});

describe('Test message DAO update status', function () {
	it('Testing method for updating status', async () => {
		const messageSid = 'sid123';

		await messageDao.updateMessage(messageSid, 'delivered');

		const res: Record<string, unknown>[] = await messageDao.getDb().any(SELECT_OUTBOUND_MESSAGE, messageSid);
		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('message__c', 'Hola amigo');
		chai.expect(outboundMsg).to.have.property('fromnumber__c', '+14777888');
		chai.expect(outboundMsg).to.have.property('tonumber__c', '+37544888777');
		chai.expect(outboundMsg).to.have.property('status__c', 'delivered');
	});
});

describe('Test message DAO select outcome message', function () {
	it('Selected outcome message where sObject is opportunity and it is the closest', async () => {
		const toNumber = '+37544888777';

		const res: Record<string, unknown>[] = await messageDao.getTheLatestOutcomeMessage(toNumber, MessageType.SMS);
		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('fromnumber__c', '+14777888');
		chai.expect(outboundMsg).to.have.property('tonumber__c', toNumber);
		chai.expect(outboundMsg).to.have.property('status__c', 'delivered');
		chai.expect(outboundMsg).to.have.property('sobject__c', 'Opportunity');
		chai.expect(outboundMsg).to.have.property('messagesid__c', 'sid125');
	});
});

describe('Test message DAO save income opportunity message', function () {
	it('Testing method for saving income opportunity message', async () => {
		const messageSid = 'msqn123';

		const outComeMessageDTO: OutcomeSMSMessageDTO = new OutcomeSMSMessageDTO(
			'Test',
			'+445566',
			'parOppo',
			'Opportunity',
			'sendby1',
			'CH',
			'false',
		);

		const message: IncomeMessage = new IncomeMessage(
			Optional.empty(),
			'445566',
			PhoneNumbers.CH,
			'Never give up',
			messageSid,
			'received',
			MessageType.SMS,
			null,
			null,
		);

		await messageDao.saveIncomeMessage(message, outComeMessageDTO);
		const res: Record<string, unknown>[] = await messageDao.getDb().any(SELECT_INBOUND_MESSAGE, messageSid);
		chai.expect(equal(res.length, 1));

		const inboundMsg: Record<string, unknown> = res[0];

		chai.expect(inboundMsg).to.have.property('message__c', 'Never give up');
		chai.expect(inboundMsg).to.have.property('fromnumber__c', '445566');
		chai.expect(inboundMsg).to.have.property('tonumber__c', PhoneNumbers.CH);
		chai.expect(inboundMsg).to.have.property('status__c', 'received');
		chai.expect(inboundMsg).to.have.property('opportunity__c', 'parOppo');
		chai.expect(inboundMsg).to.have.property('sobject__c', 'Opportunity');
		chai.expect(inboundMsg).not.to.have.property('lead__c', 'parOppo');
	});
});

describe('Test message DAO save income lead message', function () {
	it('Testing method for saving income lead message', async () => {
		const messageSid = 'msqn124';

		const outComeMessageDTO: OutcomeSMSMessageDTO = new OutcomeSMSMessageDTO(
			'Test',
			'+445566',
			'parLead',
			'Lead',
			'sendby1',
			'CH',
			'false',
		);

		const message: IncomeMessage = new IncomeMessage(
			Optional.empty(),
			'445566',
			PhoneNumbers.CH,
			'Never give up',
			messageSid,
			'received',
			MessageType.SMS,
			null,
			null,
		);

		await messageDao.saveIncomeMessage(message, outComeMessageDTO);
		const res: Record<string, unknown>[] = await messageDao.getDb().any(SELECT_INBOUND_MESSAGE, messageSid);
		chai.expect(equal(res.length, 1));

		const inboundMsg: Record<string, unknown> = res[0];

		chai.expect(inboundMsg).to.have.property('message__c', 'Never give up');
		chai.expect(inboundMsg).to.have.property('fromnumber__c', '445566');
		chai.expect(inboundMsg).to.have.property('tonumber__c', PhoneNumbers.CH);
		chai.expect(inboundMsg).to.have.property('status__c', 'received');
		chai.expect(inboundMsg).to.have.property('sobject__c', 'Lead');
		chai.expect(inboundMsg).to.have.property('lead__c', 'parLead');
		chai.expect(inboundMsg).not.to.have.property('opportunity__c', 'parLead');
	});
});

describe('Test message DAO save income case message', function () {
	it('Testing method for saving income case message', async () => {
		const messageSid = 'msqn127';

		const outComeMessageDTO: OutcomeSMSMessageDTO = new OutcomeSMSMessageDTO(
			'Test',
			'+445566',
			'parCase',
			'Case',
			'sendby1',
			'CH',
			'false',
		);

		const message: IncomeMessage = new IncomeMessage(
			Optional.empty(),
			'445566',
			PhoneNumbers.CH,
			'Never give up',
			messageSid,
			'received',
			MessageType.SMS,
			null,
			null,
		);

		await messageDao.saveIncomeMessage(message, outComeMessageDTO);
		const res: Record<string, unknown>[] = await messageDao.getDb().any(SELECT_INBOUND_MESSAGE, messageSid);
		chai.expect(equal(res.length, 1));

		const inboundMsg: Record<string, unknown> = res[0];

		chai.expect(inboundMsg).to.have.property('message__c', 'Never give up');
		chai.expect(inboundMsg).to.have.property('fromnumber__c', '445566');
		chai.expect(inboundMsg).to.have.property('tonumber__c', PhoneNumbers.CH);
		chai.expect(inboundMsg).to.have.property('status__c', 'received');
		chai.expect(inboundMsg).to.have.property('sobject__c', 'Case');
		chai.expect(inboundMsg).to.have.property('case__c', 'parCase');
		chai.expect(inboundMsg).not.to.have.property('opportunity__c', 'parCase');
		chai.expect(inboundMsg).not.to.have.property('lead__c', 'parCase');
	});
});
