import Optional from 'typescript-optional';
import { errorMessageDao } from '../../src/dao/ErrorMessageDAO';
import { ErrorMessage } from '../../src/domains/ErrorMessage';
import { DATABASE_SCHEMA, ERROR_TABLE } from '../../src/configs/env_config';
import { connection } from '../../src/dao/Connection';
import chai from 'chai';
import { equal } from 'should';
import { MessageType } from '../../src/enums/MessageType';

const TEST_SELECT_QUERY_ERROR: string = 'SELECT * FROM ' + DATABASE_SCHEMA + ERROR_TABLE + ' where errorcode=$1';

const CLEAT_ERROR_TABLE: string = 'DELETE FROM ' + DATABASE_SCHEMA + ERROR_TABLE + " where errorcode='2211'";

afterEach('Remove test data', async function () {
	console.log('CLEARING DATABASE');

	await connection.getConnection().none(CLEAT_ERROR_TABLE);
});

describe('Test Error Message DAO save error', function () {
	it('Testing method for saving error message', async () => {
		const errorMessage: ErrorMessage = new ErrorMessage(
			Optional.empty(),
			'2211',
			'error',
			'+3322',
			'+4455',
			'qwe!23asd',
			MessageType.SMS,
		);

		await errorMessageDao.saveErrorMessage(errorMessage);

		const res: Record<string, unknown>[] = await connection.getConnection().any(TEST_SELECT_QUERY_ERROR, '2211');
		chai.expect(equal(res.length, 1));

		const errorMsg: Record<string, unknown> = res[0];
		chai.expect(errorMsg).to.have.property('errorcode', '2211');
		chai.expect(errorMsg).to.have.property('type', MessageType.SMS);
		chai.expect(errorMsg).to.have.property('errormessage', 'error');
	});
});
