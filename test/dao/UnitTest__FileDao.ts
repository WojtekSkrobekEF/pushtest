import { DATABASE_SCHEMA, FILES_TABLE } from '../../src/configs/env_config';
import { File } from '../../src/domains/File';
import { WhatsAppNumbers } from '../../src/enums/WhatsAppNumbers';
import { fileDao } from '../../src/dao/FileDao';
import { connection } from '../../src/dao/Connection';
import chai from 'chai';
import { equal } from 'should';

const TEST_SELECT_FILE_BY_ID: string = 'SELECT * FROM ' + DATABASE_SCHEMA + FILES_TABLE + ' where fileid = $1';

const CLEAR_FILES_TABLE: string =
	'DELETE FROM ' + DATABASE_SCHEMA + FILES_TABLE + " where fileid in ('fileid1','fileid2','fileid3')";

afterEach('Remove test data', async function () {
	console.log('CLEARING DATABASE');

	await connection.getConnection().none(CLEAR_FILES_TABLE);
});

describe('Test File DAO, insert file', function () {
	it('Testing method for inserting file into the DB', async () => {
		const fileOneDate: Date = new Date();
		fileOneDate.setMonth(4, 30); // This is MAY 30

		const fileTwoDate: Date = new Date();
		fileTwoDate.setMonth(5, 10); // This is June 10

		const file: File = new File('fileid1', fileOneDate, WhatsAppNumbers.GB, false);
		const file2: File = new File('fileid2', fileTwoDate, WhatsAppNumbers.GB, false);
		await fileDao.insertFile(file);
		await fileDao.insertFile(file2);

		const res: Record<string, unknown>[] = await connection.getConnection().any(TEST_SELECT_FILE_BY_ID, 'fileid1');

		chai.expect(equal(res.length, 1));

		const fileDb: Record<string, unknown> = res[0];

		chai.expect(fileDb).to.have.property('fileid', 'fileid1');
		chai.expect(fileDb).to.have.property('whatsapp_server_number', '+447598954153');
		chai.expect(fileDb).to.have.property('isremoved', false);
	});
});

describe('Test File DAO, mark as removed', function () {
	it('Testing method for marking as removed file', async () => {
		const fileOneDate: Date = new Date();
		const file: File = new File('fileid1', fileOneDate, WhatsAppNumbers.GB, false);
		await fileDao.insertFile(file);

		await fileDao.markRemoved('fileid1');

		const res: Record<string, unknown>[] = await connection.getConnection().any(TEST_SELECT_FILE_BY_ID, 'fileid1');
		chai.expect(equal(res.length, 1));

		const fileDb: Record<string, unknown> = res[0];

		chai.expect(fileDb).to.have.property('fileid', 'fileid1');
		chai.expect(fileDb).to.have.property('whatsapp_server_number', '+447598954153');
		chai.expect(fileDb).to.have.property('isremoved', true);
	});
});

/*describe("Test File DAO , fetch not removed", function () {
    it("Testing method for fetching files to remove" , async () => {
        const files: File [] = [];
        const fetchedFiles: File[] = [];
        const fileOneDate: Date = new Date();
        fileOneDate.setMonth(new Date().getMonth(), new Date().getDate() - 5); // Today minus 5 days

        const fileTwoDate: Date = new Date();
        fileTwoDate.setMonth(new Date().getMonth() - 1, new Date().getDate() - 15); // Today minus month and minus 15 days

        const fileThreeDate: Date = new Date();
        fileThreeDate.setMonth(new Date().getMonth() - 1, new Date().getDate() - 9); // prev month and 9 days

        const file: File = new File("fileid1", fileOneDate, WhatsAppNumbers.GB, false); // should not be removed
        const file2: File = new File("fileid2", fileTwoDate, WhatsAppNumbers.GB, false); // should be removed
        const file3: File = new File("fileid3", fileThreeDate, WhatsAppNumbers.GB, false); // should be removed

        files.push(file, file2, file3);

        files.forEach(async (f) => await fileDao.insertFile(f));

        const res3: Record<string, unknown>[] = await fileDao.fetchFilesByRemainingDays( parseInt(FILE_REMOVAL_REMAINING_DAYS)
                                                                      , parseInt(FILE_REMOVAL_BATCH_SIZE));

        res3.forEach( (result: any) => {
            let file: File = new File( result.fileid
                                     , result.createddate
                                     , result.whatsapp_server_number
                                     , result.isremoved );
            fetchedFiles.push(file);
        });

        chai.expect(equal(fetchedFiles.length, 2));

        const filteredArray1: Record<string, unknown>[] = fetchedFiles.filter( f => f.getFileId() === file2.getFileId());
        const fetchedFileObject: any = filteredArray1[0];

        chai.expect(fetchedFileObject).to.have.property('fileId', 'fileid2');
        chai.expect(fetchedFileObject).to.have.property('waServerNumber', '+447598954153');
        chai.expect(fetchedFileObject).to.have.property('isRemoved', false);


        const filteredArray2: Record<string, unknown>[] = fetchedFiles.filter( f => f.getFileId() === file3.getFileId());

        const fetchedFileObject2: any = filteredArray2[0];

        chai.expect(fetchedFileObject2).to.have.property('fileId', 'fileid3');
        chai.expect(fetchedFileObject2).to.have.property('waServerNumber', '+447598954153');
        chai.expect(fetchedFileObject2).to.have.property('isRemoved', false);

    })
});*/
