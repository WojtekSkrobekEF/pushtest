import chai from 'chai';
import { sObjectService } from '../../src/services/SObjectService';
import { SObjects } from '../../src/enums/SObjects';

describe('Testing method for getting SMS phone number by country', function () {
	it('Should return PL phone number', () => {
		const sObject = 'Lead';

		const res: string = sObjectService.safelyGetSObject(sObject);

		chai.expect(res).to.equal(SObjects.Lead);
	});
});
