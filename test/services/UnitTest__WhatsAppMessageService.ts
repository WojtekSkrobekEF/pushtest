import { MessageStatus } from '../../src/enums/MessageStatus';
import sinon from 'sinon';
import { twilioService } from '../../src/services/TwilioService';
import { messageDao } from '../../src/dao/MessageDAO';
import chai from 'chai';
import { equal } from 'should';
import { MessageType } from '../../src/enums/MessageType';
import { OutcomeMessageDTO } from '../../src/dto/OutcomeMessageDTO';
import { whatsAppMessageService } from '../../src/services/WhatsAppMessageService';
import { DATABASE_SCHEMA, INBOUND_MESSAGE_TABLE, OUTBOUND_MESSAGE__TABLE } from '../../src/configs/env_config';
import { OutcomeMessage } from '../../src/domains/OutcomeMessage';
import Optional from 'typescript-optional';
import { IncomeMessageDTO } from '../../src/dto/IncomeMessageDTO';
import { TwillioWhatsAppNumbers } from '../../src/enums/TwillioWhatsAppNumbers';

const TEST_INSERT_QUERY_OUTCOME_MESSAGE_1: string =
	'INSERT INTO ' +
	DATABASE_SCHEMA +
	OUTBOUND_MESSAGE__TABLE +
	'(fromNumber__c, toNumber__c, message__c, messageSid__c, parent_id__c,' +
	' status__c, sobject__c, createddate , opportunity__c, isdeleted, type__c)' +
	" VALUES('+14777888','+37544888777','Hi, how are you?','sid125','QWEasd123'," +
	"'delivered', 'Opportunity', '2018-10-08 17:30:00' , 'QWEasd123', false, 'WHATSAPP')";

const TEST_INSERT_QUERY_OUTCOME_MESSAGE_2: string =
	'INSERT INTO ' +
	DATABASE_SCHEMA +
	OUTBOUND_MESSAGE__TABLE +
	'(fromNumber__c, toNumber__c, message__c, messageSid__c,' +
	' parent_id__c, status__c, sobject__c, createddate , opportunity__c, isdeleted, type__c)' +
	" VALUES('+14777888','+37544888777','Hola amigo, are you here? ','sid126'," +
	"'QWEDSA321','delivered', 'Opportunity', '2018-10-08 17:35:00' , 'QWEDSA321', false, 'WHATSAPP')";

const SELECT_FROM_OUTCOMEMESSAGE_BY_MESSAGE_SID: string =
	'SELECT * FROM ' + DATABASE_SCHEMA + OUTBOUND_MESSAGE__TABLE + ' where messageSid__c = $1';

const SELECT_FROM_INCOMEMESSAGE_BY_MESSAGE_SID: string =
	'SELECT * FROM ' + DATABASE_SCHEMA + INBOUND_MESSAGE_TABLE + ' where messageSid__c = $1';

const CLEAR_INCOME_MESSAGE: string =
	'DELETE FROM ' + DATABASE_SCHEMA + INBOUND_MESSAGE_TABLE + " where messageSid__c in ('msgsidinc','sid126','in555')";
const CLEAR_OUTCOME_MESSAGE: string =
	'DELETE FROM ' +
	DATABASE_SCHEMA +
	OUTBOUND_MESSAGE__TABLE +
	" where messageSid__c in ('sid125', 'sid126','msgsid2','msgsid3', 'msgsid4','m555','ms555')";

afterEach('Clearing database after tests', async function () {
	console.log('CLEARING DATABASE');
	sinon.restore();
	await messageDao.getDb().none(CLEAR_INCOME_MESSAGE);
	await messageDao.getDb().none(CLEAR_OUTCOME_MESSAGE);
});

describe('Testing whatsApp message sending', function () {
	it('Should save outcome whatsApp message ', async () => {
		const messageInst: Record<string, unknown> = {
			sid: 'msgsid2',
			status: MessageStatus.QUEUED,
		};

		sinon.stub(twilioService, 'sendMessageWhatsApp').resolves(messageInst);

		const msg: OutcomeMessageDTO = new OutcomeMessageDTO(
			'Test outcome message',
			'+37529889977',
			'QWEasd123',
			'Opportunity',
			'asdEWQ321',
			'US',
		);

		await whatsAppMessageService.sendMessage(msg);

		const res: Record<string, unknown>[] = await messageDao
			.getDb()
			.any(SELECT_FROM_OUTCOMEMESSAGE_BY_MESSAGE_SID, ['msgsid2']);
		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('message__c', 'Test outcome message');
		chai.expect(outboundMsg).to.have.property('sobject__c', 'Opportunity');
		chai.expect(outboundMsg).to.have.property('send_by__c', 'asdEWQ321');
		chai.expect(outboundMsg).to.have.property('fromnumber__c', TwillioWhatsAppNumbers.US);
		chai.expect(outboundMsg).to.have.property('tonumber__c', '+37529889977');
		chai.expect(outboundMsg).to.have.property('opportunity__c', 'QWEasd123');
		chai.expect(outboundMsg).to.have.property('type__c', MessageType.WhatsApp);
	});
});

describe('Testing whatsApp message update status', function () {
	it('Should update status of outcome whatsApp message', async () => {
		const status: string = MessageStatus.DELIVERED;
		const messageSid = 'msgsid2';
		const message: OutcomeMessage = new OutcomeMessage(
			Optional.empty(),
			'+375223344',
			'+37529889977',
			'WhatsApp message',
			'QWEasd123',
			'asdEWQ321',
			'Opportunity',
			MessageType.WhatsApp,
		);

		const dbRecord = await messageDao.saveOutcomeMessage(message);
		await messageDao.updateMessageSidById(messageSid, dbRecord.id);
		await whatsAppMessageService.updateStatus(messageSid, status, null);

		const res: Record<string, unknown>[] = await messageDao
			.getDb()
			.any(SELECT_FROM_OUTCOMEMESSAGE_BY_MESSAGE_SID, [messageSid]);
		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('message__c', 'WhatsApp message');
		chai.expect(outboundMsg).to.have.property('sobject__c', 'Opportunity');
		chai.expect(outboundMsg).to.have.property('status__c', status);
		chai.expect(outboundMsg).to.have.property('type__c', MessageType.WhatsApp);
	});
});

describe('Testing message service save income message', function () {
	it('Should insert record in inbound message', async () => {
		await messageDao.getDb().none(TEST_INSERT_QUERY_OUTCOME_MESSAGE_1);
		await messageDao.getDb().none(TEST_INSERT_QUERY_OUTCOME_MESSAGE_2);

		const msg: IncomeMessageDTO = new IncomeMessageDTO(
			'Income whatsapp message',
			'whatsapp:+37544888777',
			'whatsapp:+14777888',
			MessageStatus.DELIVERED,
			'msgsidinc',
		);

		await whatsAppMessageService.fetchMessage(msg);

		const res: Record<string, unknown>[] = await messageDao
			.getDb()
			.any(SELECT_FROM_INCOMEMESSAGE_BY_MESSAGE_SID, ['msgsidinc']);
		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('message__c', 'Income whatsapp message');
		chai.expect(outboundMsg).to.have.property('status__c', 'Delivered');
		chai.expect(outboundMsg).to.have.property('sobject__c', 'Opportunity');
		chai.expect(outboundMsg).to.have.property('opportunity__c', 'QWEDSA321');
		chai.expect(outboundMsg).to.not.have.property('opportunity__c', 'QWEasd321');
	});
});
