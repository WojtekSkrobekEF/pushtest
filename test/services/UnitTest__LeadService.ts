import { DATABASE_SCHEMA, LEAD_TABLE } from '../../src/configs/env_config';
import { messageDao } from '../../src/dao/MessageDAO';
import chai from 'chai';
import { leadService } from '../../src/services/LeadService';
import { leadDao } from '../../src/dao/LeadDao';
import { Lead } from '../../src/domains/Lead';
import Optional from 'typescript-optional';

const TEST_INSERT_QUERY_LEAD: string =
	'INSERT INTO ' +
	DATABASE_SCHEMA +
	LEAD_TABLE +
	"(sfid, isconverted, convertedopportunityid) values ('sfid1', 'false', 'oppo1')";

const CLEAR_LEAD_TABLE: string = 'DELETE FROM ' + DATABASE_SCHEMA + LEAD_TABLE + " where sfid='sfid1'";

afterEach('Clearing database after tests', async function () {
	console.log('CLEARING DATABASE');
	await messageDao.getDb().none(CLEAR_LEAD_TABLE);
});

describe('Testing method for getting selecting records from lead table', function () {
	it('Should return values fromm lead table ', async () => {
		await leadDao.getDb().none(TEST_INSERT_QUERY_LEAD);

		const res: Optional<Lead> = await leadService.findLeadBySfid('sfid1');
		const leadObj: Lead = res.get();

		chai.expect(leadObj.getIsConverted()).to.equal(false);
		chai.expect(leadObj.getConvertedOpportunityId()).to.equal('oppo1');
	});
});
