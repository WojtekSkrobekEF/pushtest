import chai from 'chai';
import { phoneNumberService } from '../../src/services/PhoneNumberService';
import { PhoneNumbers } from '../../src/enums/PhoneNumbers';
import { MessageServices } from '../../src/enums/MessageServices';
import { TwillioWhatsAppNumbers } from '../../src/enums/TwillioWhatsAppNumbers';

describe('Testing method for getting SMS phone number by country', function () {
	it('Should return CH phone number', () => {
		const country = 'CH';

		const res: string = phoneNumberService.safelyGetPhoneNumberByCountry(country);

		chai.expect(res).to.equal(PhoneNumbers.CH);
	});
});

describe('Testing method for getting message service by country', function () {
	it('Should return Swiss phone number', () => {
		const country = 'CH';

		const res: string = phoneNumberService.safelyGetMessageServiceByCountry(country);

		chai.expect(res).to.equal(MessageServices.CH);
	});
});

describe('Testing method for creating WhatApp phone number', function () {
	it('should return phone number with whatsapp prefix', function () {
		const number = '+436644721286';
		const validNumber: string = phoneNumberService.createWhatsAppNumber(number);
		chai.expect(validNumber).to.equal('whatsapp:+436644721286');
	});
});

describe('Testing method for removing WhatApp prefix in the phone number', function () {
	it('should return phone number without whatsapp prefix', function () {
		const number = 'whatsapp:+436644721286';
		const validNumber: string = phoneNumberService.clearWhatsAppPhoneNumber(number);
		chai.expect(validNumber).to.equal('+436644721286');
	});
});

describe('Testing method for getting WhatsApp phone number by country', function () {
	it('Should return US phone number', () => {
		const country = 'US';

		const res: string = phoneNumberService.safelyGetTwillioPhoneNumberWhatsAppByCountry(country);

		chai.expect(res).to.equal(TwillioWhatsAppNumbers.US);
	});
});

describe('Testing method for clearing phone number', function () {
	it('should return clean number', function () {
		const number = '+43(664) 472-1286';
		const validNumber: string = phoneNumberService.returnSuitableNumber(number);
		chai.expect(validNumber).to.equal('+436644721286');
	});
});
