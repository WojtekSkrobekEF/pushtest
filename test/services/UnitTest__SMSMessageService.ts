import { smsMessageService } from '../../src/services/SMSMessageService';
import { twilioService } from '../../src/services/TwilioService';
import sinon from 'sinon';
import { MessageStatus } from '../../src/enums/MessageStatus';
import { OutcomeMessage } from '../../src/domains/OutcomeMessage';
import Optional from 'typescript-optional';
import { DATABASE_SCHEMA, INBOUND_MESSAGE_TABLE, OUTBOUND_MESSAGE__TABLE } from '../../src/configs/env_config';
import { messageDao } from '../../src/dao/MessageDAO';
import chai from 'chai';
import { equal } from 'should';
import { PhoneNumbers } from '../../src/enums/PhoneNumbers';
import { MessageType } from '../../src/enums/MessageType';
import { OutcomeSMSMessageDTO } from '../../src/dto/OutcomeSMSMessageDTO';
import { IncomeMessageDTO } from '../../src/dto/IncomeMessageDTO';
import { SObjects } from '../../src/enums/SObjects';

const SELECT_FROM_OUTCOMEMESSAGE_BY_MESSAGE_SID: string =
	'SELECT * FROM ' + DATABASE_SCHEMA + OUTBOUND_MESSAGE__TABLE + ' where messageSid__c = $1';

const SELECT_FROM_INCOMEMESSAGE_BY_MESSAGE_SID: string =
	'SELECT * FROM ' + DATABASE_SCHEMA + INBOUND_MESSAGE_TABLE + ' where messageSid__c = $1';

const CLEAR_INCOME_MESSAGE: string =
	'DELETE FROM ' +
	DATABASE_SCHEMA +
	INBOUND_MESSAGE_TABLE +
	" where messageSid__c in ('msgsidinc','sid126','in555', 'inmc555')";
const CLEAR_OUTCOME_MESSAGE: string =
	'DELETE FROM ' +
	DATABASE_SCHEMA +
	OUTBOUND_MESSAGE__TABLE +
	" where messageSid__c in ('sid125', 'sid126','msgsid2','msgsid3', 'msgsid4','m555','ms555','mc555', 'ci555')";

const TEST_INSERT_QUERY_OUTCOME_MESSAGE_1: string =
	'INSERT INTO ' +
	DATABASE_SCHEMA +
	OUTBOUND_MESSAGE__TABLE +
	'(fromNumber__c, toNumber__c, message__c, messageSid__c, parent_id__c,' +
	' status__c, sobject__c, createddate , opportunity__c, isdeleted, type__c)' +
	" VALUES('+14777888','+37544888777','Hi, how are you?','sid125','QWEasd123'," +
	"'delivered', 'Opportunity', '2018-10-08 17:30:00' , 'QWEasd123', false, 'SMS')";

const TEST_INSERT_QUERY_OUTCOME_MESSAGE_2: string =
	'INSERT INTO ' +
	DATABASE_SCHEMA +
	OUTBOUND_MESSAGE__TABLE +
	'(fromNumber__c, toNumber__c, message__c, messageSid__c,' +
	' parent_id__c, status__c, sobject__c, createddate , opportunity__c, isdeleted, type__c)' +
	" VALUES('+14777888','+37544888777','Hola amigo, are you here? ','sid126'," +
	"'QWEDSA321','delivered', 'Opportunity', '2018-10-08 17:35:00' , 'QWEDSA321', false, 'SMS')";

const TEST_INSERT_QUERY_OUTCOME_MESSAGE_CASE_OBJECT: string =
	'INSERT INTO ' +
	DATABASE_SCHEMA +
	OUTBOUND_MESSAGE__TABLE +
	'(fromNumber__c, toNumber__c, message__c, messageSid__c,' +
	' parent_id__c, status__c, sobject__c, createddate , isdeleted, type__c)' +
	" VALUES('+2233','+37522888777','Text message','ms555'," +
	"'caseid','delivered', 'Case', now(), false, 'SMS')";

const TEST_INSERT_QUERY_OUTCOME_MESSAGE_CONTACT_OBJECT: string =
	'INSERT INTO ' +
	DATABASE_SCHEMA +
	OUTBOUND_MESSAGE__TABLE +
	'(fromNumber__c, toNumber__c, message__c, messageSid__c,' +
	' parent_id__c, status__c, sobject__c, createddate , isdeleted, type__c)' +
	" VALUES('+2233','+37522888777','Text message','mc555'," +
	"'contactid','delivered', 'Contact', now(), false, 'SMS')";

afterEach('Clearing database after tests', async function () {
	console.log('CLEARING DATABASE');
	sinon.restore();
	await messageDao.getDb().none(CLEAR_INCOME_MESSAGE);
	await messageDao.getDb().none(CLEAR_OUTCOME_MESSAGE);
});

describe('Testing message service save outcome message using PL number', function () {
	it('Should save outcome message without sending sms from PL number', async () => {
		const messageInst: Record<string, unknown> = {
			sid: 'msgsid2',
			status: MessageStatus.QUEUED,
		};

		sinon.stub(twilioService, 'sendMessageSMS').resolves(messageInst);

		const msg: OutcomeSMSMessageDTO = new OutcomeSMSMessageDTO(
			'Test outcome message',
			'+37529889977',
			'QWEasd123',
			'Opportunity',
			'asdEWQ321',
			'CH',
			'false',
		);

		await smsMessageService.performUC1SendMessage(msg);

		const res: Record<string, unknown>[] = await messageDao
			.getDb()
			.any(SELECT_FROM_OUTCOMEMESSAGE_BY_MESSAGE_SID, ['msgsid2']);
		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('message__c', 'Test outcome message');
		chai.expect(outboundMsg).to.have.property('sobject__c', 'Opportunity');
		chai.expect(outboundMsg).to.have.property('send_by__c', 'asdEWQ321');
		chai.expect(outboundMsg).to.have.property('fromnumber__c', PhoneNumbers.CH);
		chai.expect(outboundMsg).to.have.property('tonumber__c', '+37529889977');
		chai.expect(outboundMsg).to.have.property('opportunity__c', 'QWEasd123');
		chai.expect(outboundMsg).to.have.property('type__c', MessageType.SMS);
	});
});

describe('Testing message service update status of outcome message', function () {
	it('Should update status of outcome messae', async () => {
		const status: string = MessageStatus.DELIVERED;
		const messageSid = 'msgsid2';
		const message: OutcomeMessage = new OutcomeMessage(
			Optional.empty(),
			'+375223344',
			'+37529889977',
			'Test outcome message',
			'QWEasd123',
			'asdEWQ321',
			'Opportunity',
			MessageType.SMS,
		);
		const dbRecord = await messageDao.saveOutcomeMessage(message);
		await messageDao.updateMessageSidById(messageSid, dbRecord.id);
		await smsMessageService.performUC2UpdateMessageStatus(messageSid, status);

		const res: Record<string, unknown>[] = await messageDao
			.getDb()
			.any(SELECT_FROM_OUTCOMEMESSAGE_BY_MESSAGE_SID, [messageSid]);
		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('message__c', 'Test outcome message');
		chai.expect(outboundMsg).to.have.property('sobject__c', 'Opportunity');
		chai.expect(outboundMsg).to.have.property('status__c', status);
		chai.expect(outboundMsg).to.have.property('type__c', MessageType.SMS);
	});
});

describe('Testing message service save income message', function () {
	it('Should insert record in inbound message', async () => {
		await messageDao.getDb().none(TEST_INSERT_QUERY_OUTCOME_MESSAGE_1);
		await messageDao.getDb().none(TEST_INSERT_QUERY_OUTCOME_MESSAGE_2);

		const msg: IncomeMessageDTO = new IncomeMessageDTO(
			'Test income message',
			'+37544888777',
			'+14777888',
			MessageStatus.DELIVERED,
			'msgsidinc',
		);

		await smsMessageService.performUC3FetchMessage(msg);

		const res: Record<string, unknown>[] = await messageDao
			.getDb()
			.any(SELECT_FROM_INCOMEMESSAGE_BY_MESSAGE_SID, ['msgsidinc']);
		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('message__c', 'Test income message');
		chai.expect(outboundMsg).to.have.property('status__c', 'Delivered');
		chai.expect(outboundMsg).to.have.property('sobject__c', 'Opportunity');
		chai.expect(outboundMsg).to.have.property('opportunity__c', 'QWEDSA321');
		chai.expect(outboundMsg).to.not.have.property('opportunity__c', 'QWEasd321');
	});
});

describe('Testing message service save outcome message using CH number', function () {
	it('should send SMS from CH number', async function () {
		const messageInst: Record<string, unknown> = {
			sid: 'm555',
		};

		sinon.stub(twilioService, 'sendMessageSMS').resolves(messageInst);

		const msgDTO: OutcomeSMSMessageDTO = new OutcomeSMSMessageDTO(
			'Text message',
			'+37522',
			'parentid',
			'Opportunity',
			'sendby',
			'CH',
			'false',
		);

		await smsMessageService.performUC1SendMessage(msgDTO);

		const res: Record<string, unknown>[] = await messageDao
			.getDb()
			.any(SELECT_FROM_OUTCOMEMESSAGE_BY_MESSAGE_SID, ['m555']);
		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('message__c', 'Text message');
		chai.expect(outboundMsg).to.have.property('messagesid__c', 'm555');
		chai.expect(outboundMsg).to.have.property('tonumber__c', '+37522');
		chai.expect(outboundMsg).to.have.property('fromnumber__c', PhoneNumbers.CH);
	});
});

describe('Testing message service save income message using CH number, Case object', function () {
	it('should send SMS from CH number using CASE', async function () {
		await messageDao.getDb().any(TEST_INSERT_QUERY_OUTCOME_MESSAGE_CASE_OBJECT);

		const res: Record<string, unknown>[] = await messageDao
			.getDb()
			.any(SELECT_FROM_OUTCOMEMESSAGE_BY_MESSAGE_SID, ['ms555']);
		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('message__c', 'Text message');
		chai.expect(outboundMsg).to.have.property('messagesid__c', 'ms555');
		chai.expect(outboundMsg).to.have.property('tonumber__c', '+37522888777');
		chai.expect(outboundMsg).to.have.property('fromnumber__c', '+2233');
		chai.expect(outboundMsg).to.have.property('sobject__c', SObjects.Case);
		chai.expect(outboundMsg).to.have.property('parent_id__c', 'caseid');

		const inmsgDTO: IncomeMessageDTO = new IncomeMessageDTO(
			'Text message reply',
			'+37522888777',
			PhoneNumbers.CH,
			'received',
			'in555',
		);

		await smsMessageService.performUC3FetchMessage(inmsgDTO);

		const res1: Record<string, unknown>[] = await messageDao
			.getDb()
			.any(SELECT_FROM_INCOMEMESSAGE_BY_MESSAGE_SID, ['in555']);
		chai.expect(equal(res1.length, 1));

		const inboundMsg: Record<string, unknown> = res1[0];

		chai.expect(inboundMsg).to.have.property('message__c', 'Text message reply');
		chai.expect(inboundMsg).to.have.property('messagesid__c', 'in555');
		chai.expect(inboundMsg).to.have.property('case__c', 'caseid');
		chai.expect(inboundMsg).to.have.property('type__c', MessageType.SMS);
		chai.expect(inboundMsg).to.have.property('tonumber__c', PhoneNumbers.CH);
		chai.expect(inboundMsg).to.have.property('fromnumber__c', '+37522888777');
		chai.expect(inboundMsg).not.to.have.property('opportunity__c', 'caseid');
		chai.expect(inboundMsg).not.to.have.property('lead__c', 'caseid');
	});
});

describe('Testing message service save income message using CH number, Contact object', function () {
	it('should send SMS from CH number using CONTACT', async function () {
		await messageDao.getDb().any(TEST_INSERT_QUERY_OUTCOME_MESSAGE_CONTACT_OBJECT);

		const res: Record<string, unknown>[] = await messageDao
			.getDb()
			.any(SELECT_FROM_OUTCOMEMESSAGE_BY_MESSAGE_SID, ['mc555']);
		chai.expect(equal(res.length, 1));

		const outboundMsg: Record<string, unknown> = res[0];

		chai.expect(outboundMsg).to.have.property('message__c', 'Text message');
		chai.expect(outboundMsg).to.have.property('messagesid__c', 'mc555');
		chai.expect(outboundMsg).to.have.property('tonumber__c', '+37522888777');
		chai.expect(outboundMsg).to.have.property('fromnumber__c', '+2233');
		chai.expect(outboundMsg).to.have.property('sobject__c', SObjects.Contact);
		chai.expect(outboundMsg).to.have.property('parent_id__c', 'contactid');

		const inmsgDTO: IncomeMessageDTO = new IncomeMessageDTO(
			'Text message reply',
			'+37522888777',
			PhoneNumbers.CH,
			'received',
			'inmc555',
		);

		await smsMessageService.performUC3FetchMessage(inmsgDTO);

		const res1: Record<string, unknown>[] = await messageDao
			.getDb()
			.any(SELECT_FROM_INCOMEMESSAGE_BY_MESSAGE_SID, ['inmc555']);
		chai.expect(equal(res1.length, 1));

		const inboundMsg: Record<string, unknown> = res1[0];

		chai.expect(inboundMsg).to.have.property('message__c', 'Text message reply');
		chai.expect(inboundMsg).to.have.property('messagesid__c', 'inmc555');
		chai.expect(inboundMsg).to.have.property('contact__c', 'contactid');
		chai.expect(inboundMsg).to.have.property('type__c', MessageType.SMS);
		chai.expect(inboundMsg).to.have.property('tonumber__c', PhoneNumbers.CH);
		chai.expect(inboundMsg).to.have.property('fromnumber__c', '+37522888777');
		chai.expect(inboundMsg).not.to.have.property('opportunity__c', 'contactid');
		chai.expect(inboundMsg).not.to.have.property('lead__c', 'contactid');
	});
});
