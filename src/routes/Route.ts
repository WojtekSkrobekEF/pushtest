import bodyParser from 'body-parser';
import express from 'express';
import { controller } from '../controllers/MessageController';
import { authMiddleware } from '../controllers/middleware/AuthMiddleware';

class Route {
	public router: express.Router;

	constructor() {
		this.router = express.Router();
		this.config();
	}

	private config(): void {
		this.router.use(bodyParser.json());
		this.router.use(bodyParser.urlencoded({ extended: true }));

		this.router.post('/sendMessage', authMiddleware.authentication, controller.sendMessage);
		this.router.post('/sendMessageList', authMiddleware.authentication, controller.sendListMessage);
		this.router.post('/sendMessageWhatsApp', authMiddleware.authentication, controller.sendMessageWhatsApp);
		this.router.post('/sendMessageListWhatsApp', authMiddleware.authentication, controller.sendListWhatsAppMessages);

		// For local usage remove:
		// authMiddleware.authenticationWebhooks
		this.router.post('/fetchMessage', authMiddleware.authenticationWebhooks, controller.fetchMessage);
		this.router.post('/fetchMessageWhatsApp', authMiddleware.authenticationWebhooks, controller.fetchMessageWhatsApp);

		this.router.post('/status', authMiddleware.authenticationWebhooks, controller.updateStatus);
		this.router.post('/statusAlphaMessage', authMiddleware.authenticationWebhooks, controller.updateAlphanumStatus);
		this.router.post('/statusWhatsApp', authMiddleware.authenticationWebhooks, controller.updateStatusWhatsApp);

		this.router.get('/checkHealth', authMiddleware.authentication, controller.checkHealth);
		//  this.router.post("/whatsApp/webhook", controller.incomeMessageWhatsAppApi);
		this.router.post('/whatsApp/message', authMiddleware.authentication, controller.sendMessageWhatsAppAPI);
		this.router.post(
			'/whatsApp/messageTemplate',
			authMiddleware.authentication,
			controller.sendMessageTemplateWhatsAppAPI,
		);
		this.router.post('/whatsApp/messageList', authMiddleware.authentication, controller.sendMessageListWhatsAppApi);
		this.router.post(
			'/whatsApp/messageTemplateList',
			authMiddleware.authentication,
			controller.sendMessageTemplateListWhatsAppApi,
		);

		this.router.post('/whatsApp/download', authMiddleware.authentication, controller.downloadMedia);
		this.router.post('/whatsApp/delete', authMiddleware.authentication, controller.deleteMedia);

		this.router.post('/whatsApp/webhook/gb', controller.incomeMessageWhatsAppApiGB);
		//  this.router.post("/whatsApp/upload", upload.single("file"),  controller.upload);
	}
}

export const route: express.Router = new Route().router;
