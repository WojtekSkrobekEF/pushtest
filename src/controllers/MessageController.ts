import { Request, Response } from 'express';
import { IncomeMessageDTO } from '../dto/IncomeMessageDTO';
import { OutcomeMessageDTO } from '../dto/OutcomeMessageDTO';
import { OutcomeSMSMessageDTO } from '../dto/OutcomeSMSMessageDTO';
import { OutWhatsAppMessageDTO } from '../dto/OutWhatsAppMessageDTO';
import { OutWhatsAppMessageDTO2 } from '../dto/OutWhatsAppMessageDTO2';
import { smsMessageService } from '../services/SMSMessageService';
import { whatsAppMessageService } from '../services/WhatsAppMessageService';
import { whatsAppApiHealthService } from '../whatsAppApi/WhatsAppApiHealthService';
import { whatsAppApiMessageService } from '../whatsAppApi/WhatsAppApiMessageService';
import { WhatsAppNumbers } from '../enums/WhatsAppNumbers';
import { whatsAppApiMediaService } from '../whatsAppApi/WhatsAppApiMediaService';
import { MimeMessages } from '../enums/MimeMessages';

class MessageController {
	/////////////////////////////////////
	/// Twillio SMS Endpoints
	//

	public fetchMessage(req: Request, res: Response): void {
		const incomeSMSMessageDTO: IncomeMessageDTO = new IncomeMessageDTO(
			req.body.Body,
			req.body.From,
			req.body.To,
			req.body.SmsStatus,
			req.body.MessageSid,
		);

		smsMessageService
			.performUC3FetchMessage(incomeSMSMessageDTO)
			.then(() => {
				res.writeHead(200, { 'Content-Type': 'text/xml' });
				res.end();
			})
			.catch((error) => {
				console.log(error);
				res.writeHead(400, { 'Content-Type': 'text/xml' });
				res.end();
			});
	}

	public sendMessage(req: Request, res: Response): void {
		const outcomeSMSMessageDTO: OutcomeSMSMessageDTO = new OutcomeSMSMessageDTO(
			req.body.textMessage,
			req.body.toPhone,
			req.body.parentId,
			req.body.sObject,
			req.body.sendBy,
			req.body.country,
			req.body.isAlpha,
		);

		console.log('Outgoing message = ' + JSON.stringify(outcomeSMSMessageDTO));

		smsMessageService
			.performUC1SendMessage(outcomeSMSMessageDTO)
			.then(() => {
				res.status(200).send({ message: 'Your message have been sent' });
			})
			.catch((error) => {
				res.status(400).send({ message: error.message });
			});
	}

	public sendListMessage(req: Request, res: Response): void {
		Promise.all(smsMessageService.saveMessageList(Array.of(req.body)[0]))
			.then(() => {
				res.status(200).send({ message: 'Your message have been sent' });
			})
			.catch((error) => {
				console.log(error);
				res.status(400).send({ message: error.message });
			});
	}

	public updateStatus(req: Request, res: Response): void {
		const messageSid = req.body.MessageSid;
		const messageStatus = req.body.MessageStatus;

		smsMessageService
			.performUC2UpdateMessageStatus(messageSid, messageStatus)
			.then(() => res.sendStatus(200))
			.catch((error) => {
				console.log(error);
				res.sendStatus(400);
			});
	}

	public updateAlphanumStatus(req: Request, res: Response): void {
		const messageSid = req.body.MessageSid;
		const messageStatus = req.body.MessageStatus;
		const fromNumber = req.body.From;

		smsMessageService
			.performUC4UpdateAlphanumMessageStatus(messageSid, messageStatus, fromNumber)
			.then(() => res.sendStatus(200))
			.catch((error) => {
				console.log(error);
				res.sendStatus(400);
			});
	}

	/////////////////////////////
	///// Twillio WhatsApp Endpoints
	//

	public sendMessageWhatsApp(req: Request, res: Response): void {
		const outcomeWhatsAppMessageDTO: OutcomeMessageDTO = new OutcomeMessageDTO(
			req.body.textMessage,
			req.body.toPhone,
			req.body.parentId,
			req.body.sObject,
			req.body.sendBy,
			req.body.country,
		);

		whatsAppMessageService
			.sendMessage(outcomeWhatsAppMessageDTO)
			.then(() => {
				res.status(200).send({ message: 'Your WhatsApp message have been sent' });
			})
			.catch((error) => {
				console.log(error);
				res.status(400).send({ message: error.message });
			});
	}

	public updateStatusWhatsApp(req: Request, res: Response) {
		const messageSid = req.body.MessageSid;
		const messageStatus = req.body.MessageStatus;
		const errorCode = req.body.ErrorCode;

		whatsAppMessageService
			.updateStatus(messageSid, messageStatus, errorCode)
			.then(() => res.sendStatus(200))
			.catch((error) => {
				console.log(error);
				res.sendStatus(400);
			});
	}

	public fetchMessageWhatsApp(req: Request, res: Response) {
		const incomeWhatsAppMessageDTO: IncomeMessageDTO = new IncomeMessageDTO(
			req.body.Body,
			req.body.From,
			req.body.To,
			req.body.SmsStatus,
			req.body.MessageSid,
		);

		whatsAppMessageService
			.fetchMessage(incomeWhatsAppMessageDTO)
			.then(() => {
				res.writeHead(200, { 'Content-Type': 'text/xml' });
				res.end();
			})
			.catch((error) => {
				console.log(error);
				res.writeHead(400, { 'Content-Type': 'text/xml' });
				res.end();
			});
	}

	public sendListWhatsAppMessages(req: Request, res: Response) {
		Promise.all(whatsAppMessageService.saveMessageList(Array.of(req.body)[0]))
			.then(() => {
				res.status(200).send({ message: 'Your WhatsApp message have been sent' });
			})
			.catch((error) => {
				console.log(error);
				res.status(400).send({ message: error.message });
			});
	}

	////////////////////////////
	// WhatsApp API Endpoints
	//

	public sendMessageWhatsAppAPI(req: Request, res: Response) {
		const outMsgDto: OutWhatsAppMessageDTO = new OutWhatsAppMessageDTO(
			req.body.toPhone,
			req.body.textMessage,
			req.body.parentId,
			req.body.sObject,
			req.body.sendBy,
			req.body.country,
			req.body.namespace,
			req.body.element_name,
			req.body.params,
			req.body.code,
		);

		whatsAppApiMessageService
			.performUC1SendMessage(outMsgDto)
			.then(() => {
				res.status(200).send({ message: 'Your WhatsApp message have been sent' });
			})
			.catch((error) => {
				console.log(error);
				res.status(400).send({ message: error.message });
			});
	}

	public sendMessageTemplateWhatsAppAPI(req: Request, res: Response) {
		console.log('MessageContoller.sendMessageTemplateWhatsAppAPI ' + JSON.stringify(req.body));
		const outMsgDto: OutWhatsAppMessageDTO2 = new OutWhatsAppMessageDTO2(
			req.body.toPhone,
			req.body.textMessage,
			req.body.parentId,
			req.body.sObject,
			req.body.sendBy,
			req.body.country,
			req.body.namespace,
			req.body.element_name,
			req.body.params,
			req.body.code,
			req.body.textMessageMerged,
		);

		console.log('calling MessageContoller.performUC2SendMessageTemplate ' + JSON.stringify(outMsgDto));
		whatsAppApiMessageService
			.performUC2SendMessageTemplate(outMsgDto)
			.then(() => {
				res.status(200).send({ message: 'Your WhatsApp message have been sent' });
			})
			.catch((error) => {
				console.log(error);
				res.status(400).send({ message: error.message });
			});
	}

	public sendMessageTemplateListWhatsAppApi(req: Request, res: Response) {
		Promise.all(whatsAppApiMessageService.sendMessageTemplateList(Array.of(req.body)[0]))
			.then(() => {
				res.status(200).send({ message: 'Your WhatsApp message have been sent' });
			})
			.catch((error) => {
				console.log(error);
				res.status(400).send({ message: error.message });
			});
	}

	public sendMessageListWhatsAppApi(req: Request, res: Response) {
		Promise.all(whatsAppApiMessageService.sendMessageList(Array.of(req.body)[0]))
			.then(() => {
				res.status(200).send({ message: 'Your WhatsApp message have been sent' });
			})
			.catch((error) => {
				console.log(error);
				res.status(400).send({ message: error.message });
			});
	}

	public incomeMessageWhatsAppApiGB(req: Request, res: Response) {
		const waServerNumber: string = WhatsAppNumbers.GB;
		console.log('req received from WA ApiGB');
		console.log(req.body);
		whatsAppApiMessageService.processIncomeWebhook(req.body, waServerNumber).catch((error) => {
			console.log(error);
			res.status(400).send({ message: error.message });
		});
		res.status(200).send();
	}

	public checkHealth(req: Request, res: Response) {
		const waServerNumber: string = req.body.country;
		const result: Promise<any> = whatsAppApiHealthService.checkHealth(waServerNumber);

		result
			.then((resp) => {
				res.status(200).send(resp);
			})
			.catch((error) => {
				res.status(400).send({ message: error.message });
			});
	}

	public downloadMedia(req: Request, res: Response) {
		const fileId: string = req.body.fileId;
		const country: string = req.body.country;
		const fileType: string = req.body.fileType;

		const mimeContentType: string = whatsAppApiMediaService.processMediaType(fileType);
		const result: Promise<any> = whatsAppApiMediaService.downloadMedia(fileId, country);

		result
			.then((resp) => {
				res.setHeader('Content-Disposition', 'attachment; filename=' + '"' + fileId + '"');

				if (mimeContentType != MimeMessages.DOCUMENT) {
					res.setHeader('Content-Type', mimeContentType);
				} else {
					res.setHeader('Content-Type', resp.headers['content-type']);
				}

				res.send(Buffer.from(resp.body, 'binary'));
			})
			.catch((error) => {
				console.log('DownloadMedia error ' + new Buffer(JSON.parse(error.message.substring(6)).data).toString());
				res
					.status(400)
					.send("Requested file doesn't exist " + new Buffer(JSON.parse(error.message.substring(6)).data).toString());
			});
	}

	public deleteMedia(req: Request, res: Response) {
		const fileId: string = req.body.fileId;
		const country: string = req.body.country;
		const result: Promise<any> = whatsAppApiMediaService.deleteMedia(fileId, country);

		result
			.then(() => {
				res.status(200).send({ message: 'File were removed from AWS' });
			})
			.catch(() => {
				res.status(400).send({ message: 'Unable to remove file in AWS' });
			});
	}
}

export const controller: MessageController = new MessageController();
