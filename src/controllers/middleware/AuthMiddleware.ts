import { NextFunction, Request, Response } from 'express';
import Twilio from 'twilio';
import { PRIVATE_KEY, TWILIO_AUTH_TOKEN } from '../../configs/env_config';

class AuthMiddleware {
	public authentication(req: Request, res: Response, next: NextFunction): Response {
		const token: string = req.header('x-auth-token');

		if (token != null && authMiddleware.verifyToken(token, PRIVATE_KEY)) {
			console.log('Authentication passed');
			next();
		} else {
			return res.status(401).send('Authentication failed. Invalid token');
		}
	}

	public authenticationWebhooks(req: Request, res: Response, next: NextFunction): Response {
		const twilioSignature = req.header('X-Twilio-Signature');
		const url = 'https://' + req.get('host') + req.originalUrl;
		const params = req.body;

		if (authMiddleware.verifyRequest(twilioSignature, url, params)) {
			next();
		} else {
			return res.status(401).send('Authentication failed. Permission denied');
		}
	}

	protected verifyToken(actualToken: string, expectedToken: string): boolean {
		return actualToken === expectedToken;
	}

	protected verifyRequest(twilioSignature: string, url: string, params: Record<string, unknown>): boolean {
		return Twilio.validateRequest(TWILIO_AUTH_TOKEN, twilioSignature, url, params);
	}
}

export const authMiddleware: AuthMiddleware = new AuthMiddleware();
