export class WhatsAppContactDTO {
	private readonly phoneNumber: string;
	private readonly status: string;
	private readonly whatsAppId: string;

	constructor(phoneNumber: string, status: string, whatsAppId: string) {
		this.phoneNumber = phoneNumber;
		this.status = status;
		this.whatsAppId = whatsAppId;
	}

	public getPhoneNumber(): string {
		return this.phoneNumber;
	}

	public getStatus(): string {
		return this.status;
	}

	public getWhatsAppId(): string {
		return this.whatsAppId;
	}
}
