export class OutcomeDBWhatsAppMesageDTO {
	protected readonly message: string;
	protected readonly toNumber: string;
	protected readonly parentId: string;
	protected readonly sObject: string;
	protected readonly sendBy: string;
	protected readonly country: string;
	protected readonly messageId: string;

	constructor(
		message: string,
		toNumber: string,
		parentId: string,
		sObject: string,
		sendBy: string,
		country: string,
		messageId: string,
	) {
		this.message = message;
		this.toNumber = toNumber;
		this.parentId = parentId;
		this.sObject = sObject;
		this.sendBy = sendBy;
		this.country = country;
		this.messageId = messageId;
	}

	public getMessage(): string {
		return this.message;
	}

	public getToNumber(): string {
		return this.toNumber;
	}

	public getParentId(): string {
		return this.parentId;
	}

	public getSObject(): string {
		return this.sObject;
	}

	public getSendBy(): string {
		return this.sendBy;
	}

	public getCountry(): string {
		return this.country;
	}

	public getMessageId(): string {
		return this.messageId;
	}
}
