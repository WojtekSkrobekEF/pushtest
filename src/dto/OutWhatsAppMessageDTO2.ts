export class OutWhatsAppMessageDTO2 {
	private readonly toPhone: string;
	private readonly textMessage: string;
	private readonly parentId: string;
	private readonly sObject: string;
	private readonly sendBy: string;
	private readonly country: string;
	private readonly namespace: string;
	private readonly elementName: string;
	private readonly params: Record<string, unknown>[];
	private readonly code: string;
	private readonly textMessageMerged: string;

	constructor(
		toPhone: string,
		textMessage: string,
		parentId: string,
		sObject: string,
		sendBy: string,
		country: string,
		namespace: string,
		elementName: string,
		params: Record<string, unknown>[],
		code: string,
		textMessageMerged: string,
	) {
		this.toPhone = toPhone;
		this.textMessage = textMessage;
		this.parentId = parentId;
		this.sObject = sObject;
		this.sendBy = sendBy;
		this.country = country;
		this.namespace = namespace;
		this.elementName = elementName;
		this.params = params;
		this.code = code;
		this.textMessageMerged = textMessageMerged;
	}

	public getMessage(): string {
		return this.textMessage;
	}

	public getToPhone(): string {
		return this.toPhone;
	}

	public getParentId(): string {
		return this.parentId;
	}

	public getSObject(): string {
		return this.sObject;
	}

	public getSendBy(): string {
		return this.sendBy;
	}

	public getCountry(): string {
		return this.country;
	}

	public getNamespace(): string {
		return this.namespace;
	}

	public getElementName(): string {
		return this.elementName;
	}

	public getParams(): Record<string, unknown>[] {
		return this.params;
	}

	public getCode(): string {
		return this.code;
	}

	public getTextMessageMerged(): string {
		return this.textMessageMerged;
	}
}
