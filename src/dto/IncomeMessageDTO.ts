export class IncomeMessageDTO {
	private readonly message: string;
	private readonly fromNumber: string;
	private readonly toNumber: string;
	private readonly smsStatus: string;
	private readonly messageSid: string;

	constructor(message: string, fromNumber: string, toNumber: string, smsStatus: string, messageSid: string) {
		this.message = message;
		this.fromNumber = fromNumber;
		this.toNumber = toNumber;
		this.smsStatus = smsStatus;
		this.messageSid = messageSid;
	}

	public getMessage(): string {
		return this.message;
	}

	public getFromNumber(): string {
		return this.fromNumber;
	}

	public getToNumber(): string {
		return this.toNumber;
	}

	public getSmsStatus(): string {
		return this.smsStatus;
	}

	public getMessageSid(): string {
		return this.messageSid;
	}
}
