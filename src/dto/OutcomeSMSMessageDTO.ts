import { OutcomeMessageDTO } from './OutcomeMessageDTO';

export class OutcomeSMSMessageDTO extends OutcomeMessageDTO {
	private readonly isAlpha: string;

	constructor(
		message: string,
		toNumber: string,
		parentId: string,
		sObject: string,
		sendBy: string,
		country: string,
		isAlpha: string,
	) {
		super(message, toNumber, parentId, sObject, sendBy, country);
		this.isAlpha = isAlpha;
	}

	public getIsAlpha(): string {
		return this.isAlpha;
	}
}
