export class OutcomeMessageDTO {
	protected readonly message: string;
	protected readonly toNumber: string;
	protected readonly parentId: string;
	protected readonly sObject: string;
	protected readonly sendBy: string;
	protected readonly country: string;

	constructor(message: string, toNumber: string, parentId: string, sObject: string, sendBy: string, country: string) {
		this.message = message;
		this.toNumber = toNumber;
		this.parentId = parentId;
		this.sObject = sObject;
		this.sendBy = sendBy;
		this.country = country;
	}

	public getMessage(): string {
		return this.message;
	}

	public getToNumber(): string {
		return this.toNumber;
	}

	public getParentId(): string {
		return this.parentId;
	}

	public getSObject(): string {
		return this.sObject;
	}

	public getSendBy(): string {
		return this.sendBy;
	}

	public getCountry(): string {
		return this.country;
	}
}
