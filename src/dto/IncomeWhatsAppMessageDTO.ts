export class IncomeWhatsAppMessageDTO {
	private readonly message: string;
	private readonly fromNumber: string;
	private readonly messageId: string;
	private readonly messageContentType: string;
	private readonly fileId: string;
	private readonly waServerNumber: string;

	constructor(
		message: string,
		fromNumber: string,
		messageId: string,
		messageContentType: string,
		fileId: string,
		waServerNumber: string,
	) {
		this.message = message;
		this.fromNumber = fromNumber;
		this.messageId = messageId;
		this.messageContentType = messageContentType;
		this.fileId = fileId;
		this.waServerNumber = waServerNumber;
	}

	public getMessage(): string {
		return this.message;
	}

	public getFromNumber(): string {
		return this.fromNumber;
	}

	public getMessageId(): string {
		return this.messageId;
	}

	public getMessageContentType(): string {
		return this.messageContentType;
	}

	public getFileId(): string {
		return this.fileId;
	}

	public getWaServerNumber(): string {
		return this.waServerNumber;
	}
}
