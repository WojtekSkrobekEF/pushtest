export class MessageStatusDTO {
	private readonly messageId: string;
	private readonly status: string;
	private readonly recipientId: string;

	constructor(messageId: string, status: string, recipienId: string) {
		this.messageId = messageId;
		this.status = status;
		this.recipientId = recipienId;
	}

	public getMessageId(): string {
		return this.messageId;
	}

	public getStatus(): string {
		return this.status;
	}

	public getRecipient(): string {
		return this.recipientId;
	}
}
