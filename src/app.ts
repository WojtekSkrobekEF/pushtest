import express from 'express';
import { FILE_REMOVAL_SCHEDULE, SERVER_PORT } from './configs/env_config';
import { route } from './routes/Route';
import { schedule } from 'node-cron';
import { whatsAppApiMediaService } from './whatsAppApi/WhatsAppApiMediaService';

const app = express();
const port: string = SERVER_PORT;
app.use('/', route);
app.listen(port, () => {
	console.log('Express server listening on port ' + port);
});

schedule(FILE_REMOVAL_SCHEDULE, whatsAppApiMediaService.fileRemovalProcess);

export default app;
