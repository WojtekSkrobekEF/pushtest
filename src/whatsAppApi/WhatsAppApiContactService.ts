import Optional from 'typescript-optional';
import { Token } from '../domains/Token';
import { WhatsAppContactDTO } from '../dto/WhatsAppContactDTO';
import { whatsAppApiRequestService } from './WhatsAppApiRequestService';
import { whatsAppApiHostService } from './WhatsAppApiHostService';

class WhatsAppApiContactService {
	public async verifyContact(
		phoneNumber: string,
		token: Token,
		waServerNumber: string,
	): Promise<Optional<WhatsAppContactDTO>> {
		const host = whatsAppApiHostService.getHostByServerNumber(waServerNumber);
		const maybeContact = await whatsAppApiRequestService.checkContact(phoneNumber, token, host);
		const contactDto: WhatsAppContactDTO = this.createDto(maybeContact);

		this.validateContact(contactDto);

		return Optional.of(contactDto);
	}

	protected createDto(maybeContact: any): WhatsAppContactDTO {
		const contactData = maybeContact.contacts[0];
		const phoneNumber = contactData.input;
		const status = contactData.status;
		const wa_id = contactData.wa_id;

		return new WhatsAppContactDTO(phoneNumber, status, wa_id);
	}

	protected validateContact(contactDto: WhatsAppContactDTO) {
		if (contactDto.getStatus() !== 'valid') {
			throw new Error("Message can't be sent to requested contact");
		}
	}
}

export const waContactService: WhatsAppApiContactService = new WhatsAppApiContactService();
