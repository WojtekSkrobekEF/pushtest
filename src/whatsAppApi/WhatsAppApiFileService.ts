import { File } from '../domains/File';
import { fileDao } from '../dao/FileDao';

class WhatsAppApiFileService {
	public saveFileInfo(file: File): Promise<any> {
		return fileDao.insertFile(file);
	}
}
export const whatsAppApiFileService: WhatsAppApiFileService = new WhatsAppApiFileService();
