import Optional from 'typescript-optional';
import { Token } from '../domains/Token';
import { tokenService } from '../services/TokenService';
import { whatsAppApiRequestService } from './WhatsAppApiRequestService';
import { whatsAppApiHostService } from './WhatsAppApiHostService';

class WhatsAppApiHealthService {
	public async checkHealth(waServerNumber: string): Promise<any> {
		if (this.isEmpty(waServerNumber)) {
			throw new Error('Specify the country for the server');
		}

		const optToken: Optional<Token> = await tokenService.tokenProcess(waServerNumber);
		const token: Token = optToken.get();

		const host: string = whatsAppApiHostService.getHostByServerNumber(waServerNumber);
		return await whatsAppApiRequestService.checkHealth(token, host);
	}

	private isEmpty(str: string): boolean {
		return typeof str === 'undefined' || str === null || str.trim() === '';
	}
}

export const whatsAppApiHealthService: WhatsAppApiHealthService = new WhatsAppApiHealthService();
