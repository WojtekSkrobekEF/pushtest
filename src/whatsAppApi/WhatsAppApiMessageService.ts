import Optional from 'typescript-optional';
import { messageDao } from '../dao/MessageDAO';
import { ErrorMessage } from '../domains/ErrorMessage';
import { File } from '../domains/File';
import { IncomeMessage } from '../domains/IncomeMessage';
import { Lead } from '../domains/Lead';
import { OutcomeMessage } from '../domains/OutcomeMessage';
import { Token } from '../domains/Token';
import { IncomeWhatsAppMessageDTO } from '../dto/IncomeWhatsAppMessageDTO';
import { MessageStatusDTO } from '../dto/MessageStatusDTO';
import { OutcomeMessageDTO } from '../dto/OutcomeMessageDTO';
import { OutWhatsAppMessageDTO } from '../dto/OutWhatsAppMessageDTO';
import { OutWhatsAppMessageDTO2 } from '../dto/OutWhatsAppMessageDTO2';
import { WhatsAppContactDTO } from '../dto/WhatsAppContactDTO';
import { MessageContentType } from '../enums/MessageContentType';
import { MessageStatus } from '../enums/MessageStatus';
import { MessageType } from '../enums/MessageType';
import { SObjects } from '../enums/SObjects';
import { errorService } from '../services/ErrorService';
import { leadService } from '../services/LeadService';
import { phoneNumberService } from '../services/PhoneNumberService';
import { sObjectService } from '../services/SObjectService';
import { tokenService } from '../services/TokenService';
import { waContactService } from './WhatsAppApiContactService';
import { whatsAppApiFileService } from './WhatsAppApiFileService';
import { whatsAppApiHostService } from './WhatsAppApiHostService';
import { whatsAppApiMediaService } from './WhatsAppApiMediaService';
import { whatsAppApiRequestService } from './WhatsAppApiRequestService';

class WhatsAppApiMessageService {
	public async performUC1SendMessage(outMsgDto: OutWhatsAppMessageDTO): Promise<any> {
		return new Promise<any>(async (resolve, reject) => {
			const fromNumber: string = phoneNumberService.safelyGetPhoneNumberWhatsAppByCountry(outMsgDto.getCountry());
			const validToNumber: string = phoneNumberService.returnSuitableNumber(outMsgDto.getToPhone());

			const message: OutcomeMessage = new OutcomeMessage(
				Optional.empty(),
				fromNumber,
				validToNumber,
				outMsgDto.getMessage(),
				outMsgDto.getParentId(),
				outMsgDto.getSendBy(),
				sObjectService.safelyGetSObject(outMsgDto.getSObject()),
				MessageType.WhatsApp,
			);

			const dbRecord = await this.saveOutcomeWhatsAppMessage(message);

			const messageId = this.sendMessage(outMsgDto);

			messageId
				.then(async (msgId) => {
					await this.updateOutcomeMessageSidById(msgId, dbRecord.id);
					resolve();
				})
				.catch(async (rej) => {
					console.log(rej);
					await errorService.createError(rej.status, rej.code, rej.message, message);
					await this.updateOutcomeMessageStatusErrorById(
						MessageStatus.COULD_NOT_BE_DELIVERED,
						rej.message,
						dbRecord.id,
					);
					return reject(rej);
				});
		});
	}

	public async performUC2SendMessageTemplate(outMsgDtoFull: OutWhatsAppMessageDTO2): Promise<any> {
		const outMsgDto: OutWhatsAppMessageDTO = new OutWhatsAppMessageDTO(
			outMsgDtoFull.getToPhone(),
			outMsgDtoFull.getMessage(),
			outMsgDtoFull.getParentId(),
			outMsgDtoFull.getSObject(),
			outMsgDtoFull.getSendBy(),
			outMsgDtoFull.getCountry(),
			outMsgDtoFull.getNamespace(),
			outMsgDtoFull.getElementName(),
			outMsgDtoFull.getParams(),
			outMsgDtoFull.getCode(),
		);

		return new Promise<any>(async (resolve, reject) => {
			const fromNumber: string = phoneNumberService.safelyGetPhoneNumberWhatsAppByCountry(outMsgDto.getCountry());
			const validToNumber: string = phoneNumberService.returnSuitableNumber(outMsgDto.getToPhone());

			let textMsgToBeSaved = '';

			if (outMsgDtoFull.getTextMessageMerged() && outMsgDtoFull.getTextMessageMerged().length > 0) {
				textMsgToBeSaved = outMsgDtoFull.getTextMessageMerged();
			} else {
				textMsgToBeSaved = outMsgDtoFull.getMessage();
			}

			const message: OutcomeMessage = new OutcomeMessage(
				Optional.empty(),
				fromNumber,
				validToNumber,
				textMsgToBeSaved,
				outMsgDto.getParentId(),
				outMsgDto.getSendBy(),
				sObjectService.safelyGetSObject(outMsgDto.getSObject()),
				MessageType.WhatsApp,
			);

			console.log('performUC2SendMessageTemplate inserting in DB ' + JSON.stringify(message));
			const dbRecord = await this.saveOutcomeWhatsAppMessage(message);

			console.log('performUC2SendMessageTemplate sending to WA Api ' + JSON.stringify(outMsgDto));
			const messageId = this.sendTemplateMessage(outMsgDto);

			messageId
				.then(async (msgId) => {
					await this.updateOutcomeMessageSidById(msgId, dbRecord.id);
					resolve();
				})
				.catch(async (rej) => {
					console.log(rej);
					await errorService.createError(rej.status, rej.code, rej.message, message);
					await this.updateOutcomeMessageStatusErrorById(
						MessageStatus.COULD_NOT_BE_DELIVERED,
						rej.message,
						dbRecord.id,
					);
					return reject(rej);
				});
		});
	}

	public async processIncomeWebhook(req: any, waServerNumber: string) {
		const messageData: Record<string, unknown>[] = req.messages;
		const statusData: Record<string, unknown>[] = req.statuses;
		const errorsData: Record<string, unknown>[] = req.errors;

		if (!this.isEmpty(messageData)) {
			const messageDataDetails: any = messageData[0];

			const incomeWhatsAppMessageDto: IncomeWhatsAppMessageDTO = this.createIncomeMessageByType(
				messageDataDetails,
				waServerNumber,
			);

			await whatsAppApiMessageService.saveIncomeMessageProcess(incomeWhatsAppMessageDto);
		}

		if (!this.isEmpty(statusData)) {
			const statusDataDetails: any = statusData[0];
			const waMessageStatusDto: MessageStatusDTO = new MessageStatusDTO(
				statusDataDetails.id,
				statusDataDetails.status,
				statusDataDetails.recipient_id,
			);
			await whatsAppApiMessageService.updateStatusMessage(waMessageStatusDto);
		}

		if (!this.isEmpty(errorsData)) {
			console.log(errorsData);
		}
	}

	public createIncomeMessageByType(messageDataDetails: any, waServerNumber: string): IncomeWhatsAppMessageDTO {
		// Test whether media contains text
		switch (messageDataDetails.type) {
			case MessageContentType.TEXT:
				return this.processIncomeText(messageDataDetails, waServerNumber);
			case MessageContentType.IMAGE:
				return whatsAppApiMediaService.processIncomeImage(messageDataDetails, waServerNumber);
			case MessageContentType.VIDEO:
				return whatsAppApiMediaService.processIncomeVideo(messageDataDetails, waServerNumber);
			case MessageContentType.DOCUMENT:
				return whatsAppApiMediaService.processIncomeDocument(messageDataDetails, waServerNumber);
			case MessageContentType.VOICE:
				return whatsAppApiMediaService.processIncomeVoice(messageDataDetails, waServerNumber);
		}
	}

	public processIncomeText(messageDataDetails: any, waServerNumber: string): IncomeWhatsAppMessageDTO {
		return new IncomeWhatsAppMessageDTO(
			messageDataDetails.text.body,
			phoneNumberService.returnSuitableNumber(messageDataDetails.from),
			messageDataDetails.id,
			MessageContentType.TEXT,
			null,
			waServerNumber,
		);
	}

	public async sendMessage(outWhatsAppMsgDTO: OutWhatsAppMessageDTO): Promise<string> {
		const waServerNumber: string = phoneNumberService.safelyGetPhoneNumberWhatsAppByCountry(
			outWhatsAppMsgDTO.getCountry(),
		);
		const optToken: Optional<Token> = await tokenService.tokenProcess(waServerNumber);
		const token: Token = optToken.get();

		const optContact: Optional<WhatsAppContactDTO> = await waContactService.verifyContact(
			outWhatsAppMsgDTO.getToPhone(),
			token,
			waServerNumber,
		);

		const contact: WhatsAppContactDTO = optContact.get();

		const host: string = whatsAppApiHostService.getHostByServerNumber(waServerNumber);
		const response = await whatsAppApiRequestService.sendMessage(
			contact.getWhatsAppId(),
			outWhatsAppMsgDTO.getMessage(),
			token,
			host,
		);

		return this.extractData(response);
	}

	public async sendTemplateMessage(outWhatsAppMsgDTO: OutWhatsAppMessageDTO): Promise<string> {
		const waServerNumber: string = phoneNumberService.safelyGetPhoneNumberWhatsAppByCountry(
			outWhatsAppMsgDTO.getCountry(),
		);
		const optToken: Optional<Token> = await tokenService.tokenProcess(waServerNumber);
		const token: Token = optToken.get();

		const optContact: Optional<WhatsAppContactDTO> = await waContactService.verifyContact(
			outWhatsAppMsgDTO.getToPhone(),
			token,
			waServerNumber,
		);
		const contact: WhatsAppContactDTO = optContact.get();

		const host: string = whatsAppApiHostService.getHostByServerNumber(waServerNumber);

		const response = await whatsAppApiRequestService.sendTemplate(
			outWhatsAppMsgDTO.getElementName(),
			outWhatsAppMsgDTO.getNamespace(),
			outWhatsAppMsgDTO.getParams(),
			outWhatsAppMsgDTO.getCode(),
			contact.getWhatsAppId(),
			token,
			host,
		);

		return this.extractData(response);
	}

	public sendMessageList(messageList: any[]): Array<Promise<any>> {
		const arrayPromise: Array<Promise<any>> = new Array(messageList.length);

		messageList.forEach((outMsg) => {
			const outMsgDto: OutWhatsAppMessageDTO = new OutWhatsAppMessageDTO(
				outMsg.toPhone,
				outMsg.textMessage,
				outMsg.parentId,
				outMsg.sObject,
				outMsg.sendBy,
				outMsg.country,
				outMsg.namespace,
				outMsg.element_name,
				outMsg.params,
				outMsg.code,
			);

			arrayPromise.push(this.performUC1SendMessage(outMsgDto));
		});

		return arrayPromise;
	}

	public sendMessageTemplateList(messageList: any[]): Array<Promise<any>> {
		const arrayPromise: Array<Promise<any>> = new Array(messageList.length);

		messageList.forEach((outMsg) => {
			const outMsgDto: OutWhatsAppMessageDTO2 = new OutWhatsAppMessageDTO2(
				outMsg.toPhone,
				outMsg.textMessage,
				outMsg.parentId,
				outMsg.sObject,
				outMsg.sendBy,
				outMsg.country,
				outMsg.namespace,
				outMsg.element_name,
				outMsg.params,
				outMsg.code,
				outMsg.textMessageMerged,
			);

			arrayPromise.push(this.performUC2SendMessageTemplate(outMsgDto));
		});

		return arrayPromise;
	}

	public saveIncomeWhatsAppMessage(message: IncomeMessage, outcomeMessageDTO: OutcomeMessageDTO): Promise<any> {
		return messageDao.saveIncomeMessage(message, outcomeMessageDTO);
	}

	public async safelyGetLastOutComeMessage(dto: IncomeWhatsAppMessageDTO): Promise<OutcomeMessageDTO> {
		const res = await messageDao.getTheLatestOutcomeMessage(
			phoneNumberService.returnSuitableNumber(dto.getFromNumber()),
			MessageType.WhatsApp,
		);

		if (res.length === 1) {
			return this.mapToDTO(res);
		} else {
			const msg: string = 'Outcome message from ' + dto.getFromNumber() + ' were not found!';

			const errMessage: ErrorMessage = new ErrorMessage(
				Optional.empty(),
				'404',
				msg,
				dto.getWaServerNumber(),
				dto.getFromNumber(),
				null,
				MessageType.WhatsApp,
			);
			await errorService.saveError(errMessage);

			throw new Error(msg);
		}
	}

	protected updateStatusMessage(messageStatusDto: MessageStatusDTO): Promise<any> {
		return messageDao.updateMessage(messageStatusDto.getMessageId(), messageStatusDto.getStatus());
	}

	protected async saveIncomeMessageProcess(inDto: IncomeWhatsAppMessageDTO) {
		if (inDto.getMessageContentType() !== MessageContentType.TEXT) {
			await this.saveIncomeMediaMessageProcess(inDto);
		} else {
			await this.saveIncomeTextMessageProcess(inDto);
		}
	}

	protected async saveIncomeMediaMessageProcess(inDto: IncomeWhatsAppMessageDTO) {
		const outcomeMessage: OutcomeMessageDTO = await this.safelyGetLastOutComeMessage(inDto);
		const toNumber: string = phoneNumberService.returnSuitableNumber(inDto.getFromNumber());
		const file: File = new File(inDto.getFileId(), new Date(), inDto.getWaServerNumber(), false);

		await this.createAndSaveIncomeMediaMessage(outcomeMessage, inDto, toNumber);
		await whatsAppApiFileService.saveFileInfo(file);
	}

	protected async saveIncomeTextMessageProcess(inDto: IncomeWhatsAppMessageDTO) {
		const outcomeMessage: OutcomeMessageDTO = await this.safelyGetLastOutComeMessage(inDto);
		const toNumber: string = phoneNumberService.returnSuitableNumber(inDto.getFromNumber());
		const leadObj: Optional<Lead> = await leadService.findLeadBySfid(outcomeMessage.getParentId());

		if (leadObj.isPresent) {
			const lead: Lead = leadObj.get();

			if (lead.getIsConverted()) {
				await this.changeAndSaveIncomeMessage(outcomeMessage, inDto, lead, toNumber);
			} else {
				await this.createAndSaveIncomeMessage(outcomeMessage, inDto, toNumber);
			}
		} else {
			await this.createAndSaveIncomeMessage(outcomeMessage, inDto, toNumber);
		}
	}

	protected async createAndSaveIncomeMediaMessage(
		outcomeMessage: OutcomeMessageDTO,
		inWhatsAppMsgDTO: IncomeWhatsAppMessageDTO,
		toNumber: string,
	) {
		const message: IncomeMessage = new IncomeMessage(
			Optional.empty(),
			toNumber,
			inWhatsAppMsgDTO.getWaServerNumber(),
			inWhatsAppMsgDTO.getMessage(),
			inWhatsAppMsgDTO.getMessageId(),
			MessageStatus.RECEIVED,
			MessageType.WhatsApp,
			inWhatsAppMsgDTO.getMessageContentType(),
			inWhatsAppMsgDTO.getFileId(),
		);

		await this.saveIncomeWhatsAppMessage(message, outcomeMessage);
	}

	protected saveOutcomeWhatsAppMessage(message: OutcomeMessage): Promise<any> {
		return messageDao.saveOutcomeMessage(message);
	}

	protected updateOutcomeMessageSidById(messageSid: string, recordId: number): Promise<any> {
		return messageDao.updateMessageSidById(messageSid, recordId);
	}

	protected updateOutcomeMessageStatusErrorById(status: string, errorMessage: string, recordId: number): Promise<any> {
		return messageDao.updateMessageStatusErrorById(status, errorMessage, recordId);
	}

	protected async changeAndSaveIncomeMessage(
		outcomeMessage: OutcomeMessageDTO,
		inWhatsAppMsgDTO: IncomeWhatsAppMessageDTO,
		lead: Lead,
		toNumber: string,
	) {
		/**
		 * if found record in DB has lead.isConverted === true
		 * this means that the object was converted from Lead --> Opportunity
		 * Here we updating outcomeSMSMessageDTO( sObject and parentId fields)
		 * Creating new income message with
		 * updated details soject__c = Opportunity
		 * and opportunity__c = convertedopportunityid
		 */
		const updatedOutcomeMessage: OutcomeMessageDTO = new OutcomeMessageDTO(
			outcomeMessage.getMessage(),
			outcomeMessage.getToNumber(),
			lead.getConvertedOpportunityId(),
			SObjects.Opportunity,
			outcomeMessage.getSendBy(),
			outcomeMessage.getCountry(),
		);

		const message: IncomeMessage = new IncomeMessage(
			Optional.empty(),
			toNumber,
			inWhatsAppMsgDTO.getWaServerNumber(),
			inWhatsAppMsgDTO.getMessage(),
			inWhatsAppMsgDTO.getMessageId(),
			MessageStatus.RECEIVED,
			MessageType.WhatsApp,
			null,
			null,
		);

		await this.saveIncomeWhatsAppMessage(message, updatedOutcomeMessage);
	}

	protected async createAndSaveIncomeMessage(
		outcomeMessage: OutcomeMessageDTO,
		inWhatsAppMsgDTO: IncomeWhatsAppMessageDTO,
		toNumber: string,
	) {
		const message: IncomeMessage = new IncomeMessage(
			Optional.empty(),
			toNumber,
			inWhatsAppMsgDTO.getWaServerNumber(),
			inWhatsAppMsgDTO.getMessage(),
			inWhatsAppMsgDTO.getMessageId(),
			MessageStatus.RECEIVED,
			MessageType.WhatsApp,
			null,
			null,
		);

		await this.saveIncomeWhatsAppMessage(message, outcomeMessage);
	}

	private extractData(data: any): string {
		const messageData = data.messages[0];
		return messageData.id;
	}

	private isEmpty(input: Record<string, unknown>[]): boolean {
		return input === undefined || input.length === 0;
	}

	private mapToDTO(res: Record<string, unknown>[]): OutcomeMessageDTO {
		const outMsgObject: any = res[0];
		return new OutcomeMessageDTO(
			outMsgObject.message__c,
			outMsgObject.tonumber__c,
			outMsgObject.parent_id__c,
			outMsgObject.sobject__c,
			outMsgObject.send_by__c,
			null,
		);
	}
}

export const whatsAppApiMessageService: WhatsAppApiMessageService = new WhatsAppApiMessageService();
