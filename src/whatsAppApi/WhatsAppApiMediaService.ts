import Optional from 'typescript-optional';
import { Token } from '../domains/Token';
import { IncomeWhatsAppMessageDTO } from '../dto/IncomeWhatsAppMessageDTO';
import { phoneNumberService } from '../services/PhoneNumberService';
import { whatsAppApiRequestService } from './WhatsAppApiRequestService';
import { MessageContentType } from '../enums/MessageContentType';
import { MimeMessages } from '../enums/MimeMessages';
import { tokenService } from '../services/TokenService';
import { whatsAppApiHostService } from './WhatsAppApiHostService';
import { FILE_REMOVAL_BATCH_SIZE, FILE_REMOVAL_REMAINING_DAYS } from '../configs/env_config';
import { fileDao } from '../dao/FileDao';
import { File } from '../domains/File';
import { fileObjectMapper } from '../objectMappers/FileObjectMapper';
import { errorService } from '../services/ErrorService';
import { FileError } from '../domains/FileError';

class WhatsAppApiMediaService {
	public processIncomeImage(messageDataDetails: any, waServerNumber: string): IncomeWhatsAppMessageDTO {
		return new IncomeWhatsAppMessageDTO(
			messageDataDetails.type,
			phoneNumberService.returnSuitableNumber(messageDataDetails.from),
			messageDataDetails.id,
			messageDataDetails.type,
			messageDataDetails.image.id,
			waServerNumber,
		);
	}

	public processIncomeVideo(messageDataDetails: any, waServerNumber: string): IncomeWhatsAppMessageDTO {
		return new IncomeWhatsAppMessageDTO(
			messageDataDetails.type,
			phoneNumberService.returnSuitableNumber(messageDataDetails.from),
			messageDataDetails.id,
			messageDataDetails.type,
			messageDataDetails.video.id,
			waServerNumber,
		);
	}

	public processIncomeDocument(messageDataDetails: any, waServerNumber: string): IncomeWhatsAppMessageDTO {
		return new IncomeWhatsAppMessageDTO(
			messageDataDetails.type,
			phoneNumberService.returnSuitableNumber(messageDataDetails.from),
			messageDataDetails.id,
			messageDataDetails.type,
			messageDataDetails.document.id,
			waServerNumber,
		);
	}

	public processIncomeVoice(messageDataDetails: any, waServerNumber: string): IncomeWhatsAppMessageDTO {
		return new IncomeWhatsAppMessageDTO(
			messageDataDetails.type,
			phoneNumberService.returnSuitableNumber(messageDataDetails.from),
			messageDataDetails.id,
			messageDataDetails.type,
			messageDataDetails.voice.id,
			waServerNumber,
		);
	}

	public async downloadMedia(fileId: string, country: string): Promise<any> {
		const waServerNumber: string = phoneNumberService.safelyGetPhoneNumberWhatsAppByCountry(country);
		const optToken: Optional<Token> = await tokenService.tokenProcess(waServerNumber);
		const token: Token = optToken.get();
		const host: string = whatsAppApiHostService.getHostByServerNumber(waServerNumber);
		return await whatsAppApiRequestService.downloadMedia(fileId, token, host);
	}

	public async deleteMedia(fileId: string, country: string): Promise<any> {
		const waServerNumber: string = phoneNumberService.safelyGetPhoneNumberWhatsAppByCountry(country);
		const optToken: Optional<Token> = await tokenService.tokenProcess(waServerNumber);
		const token: Token = optToken.get();
		const host: string = whatsAppApiHostService.getHostByServerNumber(waServerNumber);
		return await whatsAppApiRequestService.deleteMedia(fileId, token, host);
	}

	public processMediaType(mediaType: string): string {
		switch (mediaType) {
			case MessageContentType.IMAGE:
				return MimeMessages.IMAGE;
			case MessageContentType.VOICE:
				return MimeMessages.VOICE;
			case MessageContentType.VIDEO:
				return MimeMessages.VIDEO;
			case MessageContentType.DOCUMENT:
				return MimeMessages.DOCUMENT;

			default:
				throw new Error('Please specify media message type');
		}
	}

	public async fileRemovalProcess(): Promise<any> {
		console.log('removal process started to work');
		const batchSize = Number(FILE_REMOVAL_BATCH_SIZE);
		const remainingDays = Number(FILE_REMOVAL_REMAINING_DAYS);

		for (;;) {
			const res: Record<string, unknown>[] = await fileDao.fetchFilesByRemainingDays(remainingDays, batchSize);
			if (res.length === 0) {
				break;
			}

			const files: Set<File> = whatsAppApiMediaService.convertFiles(res);
			await whatsAppApiMediaService.removeInAWS(files);
			await whatsAppApiMediaService.markAsRemoved(files);
		}
		console.log('Finished removal process');
	}

	public async markAsRemoved(files: Set<File>) {
		files.forEach(async (file) => {
			await fileDao.markRemoved(file.getFileId());
		});
	}

	public async removeInAWS(files: Set<File>) {
		files.forEach(async (file) => {
			const optToken: Optional<Token> = await tokenService.tokenProcess(file.getWaServerNumber());
			const token: Token = optToken.get();
			const host: string = whatsAppApiHostService.getHostByServerNumber(file.getWaServerNumber());

			whatsAppApiRequestService.deleteMedia(file.getFileId(), token, host).catch(async () => {
				const fileError: FileError = new FileError(
					file.getFileId(),
					'Unable to remove file in AWS',
					file.getWaServerNumber(),
				);
				await errorService.saveErrorFile(fileError);
			});
		});
	}

	public convertFiles(arr: any): Set<File> {
		const files: Set<File> = new Set<File>();

		arr.forEach((res: any) => {
			const file: File = fileObjectMapper.mapFile(res);
			files.add(file);
		});
		return files;
	}
}

export const whatsAppApiMediaService: WhatsAppApiMediaService = new WhatsAppApiMediaService();
