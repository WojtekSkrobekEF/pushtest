import Optional from 'typescript-optional';
import { Token } from '../domains/Token';
import { whatsAppApiHostService } from './WhatsAppApiHostService';
import { whatsAppApiRequestService } from './WhatsAppApiRequestService';
import { tokenObjectMaper } from '../objectMappers/TokenObjectMapper';

class WhatsAppApiTokenService {
	public async fetchAdminToken(waServerNumber: string): Promise<Optional<Token>> {
		/**
		 *  const maybeHash = await mockWhatsAppApiRequest.loginAdminMock(hash);
		 *  const data = JSON.parse(maybeHash.body);
		 *  const tokenData = data.users[0];
		 *
		 *  Code above is needed only for mocks
		 */

		const hash = whatsAppApiHostService.generateHashByServerNumberAndCredentials(waServerNumber);
		const host = whatsAppApiHostService.getHostByServerNumber(waServerNumber);

		const maybeHash = await whatsAppApiRequestService.loginAdmin(hash, host);
		const tokenData = maybeHash.users[0];
		const token: Token = tokenObjectMaper.mapAWSToken(tokenData, waServerNumber);
		return Optional.of(token);
	}
}

export const whatsAppApiTokenService: WhatsAppApiTokenService = new WhatsAppApiTokenService();
