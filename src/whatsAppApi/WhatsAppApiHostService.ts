import { WhatsAppNumbers } from '../enums/WhatsAppNumbers';
import { WHATSAPP_ADMIN_LOGIN_GB, WHATSAPP_ADMIN_PASS_GB, WHATSAPP_API_HOST_GB } from '../configs/env_config';

class WhatsAppApiHostService {
	public generateHashByServerNumberAndCredentials(waServerNumber: string) {
		return this.getCredentialsByNumber(waServerNumber);
	}

	public getHostByServerNumber(waServerNumber: string): string {
		switch (waServerNumber) {
			case WhatsAppNumbers.GB:
				return WHATSAPP_API_HOST_GB;

			default:
				throw new Error('Please specify the phone number to get WhatsApp HOST URL');
		}
	}

	private getCredentialsByNumber(waServerNumber: string): string {
		switch (waServerNumber) {
			case WhatsAppNumbers.GB:
				return this.generateHashWithCreds(WHATSAPP_ADMIN_LOGIN_GB, WHATSAPP_ADMIN_PASS_GB);

			default:
				throw new Error('Please specify the phone number to get WhatsApp creds');
		}
	}

	private generateHashWithCreds(user: string, pass: string): string {
		return Buffer.from(user + ':' + pass).toString('base64');
	}
}
export const whatsAppApiHostService: WhatsAppApiHostService = new WhatsAppApiHostService();
