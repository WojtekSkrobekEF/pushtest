import * as request from 'request-promise-native';
import { RequestPromise } from 'request-promise-native';
import { Token } from '../domains/Token';

class WhatsAppApiRequestService {
	public sendMessage(whatsAppId: string, message: string, token: Token, host: string): RequestPromise<any> {
		return request.post({
			body: {
				recipient_type: 'individual',
				text: {
					body: message,
				},
				to: whatsAppId,
				type: 'text',
			},
			headers: {
				Authorization: 'Bearer ' + token.getToken(),
				'Content-Type': 'application/json',
			},
			json: true,
			// rejectUnauthorized: false,
			url: host + '/v1/messages',
		});
	}

	public sendTemplate(
		elementName: string,
		InNamespace: string,
		params: Record<string, unknown>[],
		InCode: string,
		whatsAppId: string,
		token: Token,
		host: string,
	): RequestPromise<any> {
		return request.post({
			body: {
				hsm: {
					element_name: elementName,
					language: {
						code: InCode,
						policy: 'deterministic',
					},
					localizable_params: params,
					namespace: InNamespace,
				},
				to: whatsAppId,
				type: 'hsm',
			},
			headers: {
				Authorization: 'Bearer ' + token.getToken(),
				'Content-Type': 'application/json',
			},
			json: true,
			// rejectUnauthorized: false,
			url: host + '/v1/messages',
		});
	}

	public loginAdmin(hash: string, host: string): RequestPromise<any> {
		return request.post({
			headers: {
				// admin loginAdmin already registered
				Authorization: 'Basic ' + hash,
				'Content-Type': 'application/json',
			},
			json: true,
			// rejectUnauthorized: false,
			url: host + '/v1/users/login',
		});
	}

	public async checkHealth(token: Token, host: string): Promise<any> {
		return request.get({
			headers: {
				Authorization: 'Bearer ' + token.getToken(),
				'Content-Type': 'application/json',
			},
			json: true,
			url: host + '/v1/health',
			// rejectUnauthorized: false
		});
	}

	public checkContact(phoneNumber: string, token: Token, host: string): RequestPromise<any> {
		return request.post({
			body: {
				blocking: 'wait',
				contacts: [phoneNumber],
			},
			headers: {
				Authorization: 'Bearer ' + token.getToken(),
				'Content-Type': 'application/json',
			},
			json: true,
			// rejectUnauthorized: false,
			url: host + '/v1/contacts',
		});
	}

	public downloadMedia(fileId: string, token: Token, host: string): RequestPromise<any> {
		return request.get({
			headers: {
				Authorization: 'Bearer ' + token.getToken(),
				'Content-Type': 'application/json',
			},
			resolveWithFullResponse: true,
			encoding: null,
			// rejectUnauthorized: false,
			url: host + '/v1/media/' + fileId,
		});
	}

	public deleteMedia(fileId: string, token: Token, host: string): RequestPromise<any> {
		return request.del({
			headers: {
				Authorization: 'Bearer ' + token.getToken(),
				'Content-Type': 'application/json',
			},

			encoding: null,
			// rejectUnauthorized: false,
			url: host + '/v1/media/' + fileId,
		});
	}
}

export const whatsAppApiRequestService: WhatsAppApiRequestService = new WhatsAppApiRequestService();
