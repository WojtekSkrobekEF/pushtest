import pgPromise from 'pg-promise';
import { DATABASE_SCHEMA, ERROR_TABLE } from '../configs/env_config';
import { ErrorMessage } from '../domains/ErrorMessage';
import { connection } from './Connection';
import { FileError } from '../domains/FileError';
import { MessageType } from '../enums/MessageType';

class ErrorMessageDAO {
	private readonly db: pgPromise.IDatabase<Error>;

	private readonly INSERT_QUERY_ERROR: string =
		'INSERT INTO ' +
		DATABASE_SCHEMA +
		ERROR_TABLE +
		'(errorCode, errorMessage, toNumber__c, fromNumber__c, sobject, createddate, type)' +
		' VALUES($1, $2, $3, $4, $5, now(), $6)';

	private readonly INSERT_QUERY_ERROR_FILE: string =
		'INSERT INTO ' +
		DATABASE_SCHEMA +
		ERROR_TABLE +
		'(errorMessage, createddate, type, whatsapp_server_number, file_id)' +
		' VALUES($1,now(),$2,$3,$4)';

	constructor() {
		this.db = connection.getConnection();
	}

	public saveErrorMessage(errorMessage: ErrorMessage): Promise<any> {
		return this.db.none(this.INSERT_QUERY_ERROR, [
			errorMessage.getErrorCode(),
			errorMessage.getErrorMessage(),
			errorMessage.getToNumber(),
			errorMessage.getFromNumber(),
			errorMessage.getSObject(),
			errorMessage.getType(),
		]);
	}

	public saveErrorFile(fileError: FileError): Promise<any> {
		return this.db.none(this.INSERT_QUERY_ERROR_FILE, [
			fileError.getErrorMessage(),
			MessageType.WhatsApp,
			fileError.getWhatsAppServerNumber(),
			fileError.getFileId(),
		]);
	}
}

export const errorMessageDao: ErrorMessageDAO = new ErrorMessageDAO();
