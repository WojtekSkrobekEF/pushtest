import pgPromise from 'pg-promise';
import { DATABASE_SCHEMA, TOKEN_TABLE } from '../configs/env_config';
import { Token } from '../domains/Token';
import { connection } from './Connection';

class TokenDao {
	private readonly db: pgPromise.IDatabase<Token>;
	private readonly SELECT_LATEST_TOKEN_BY_NUMBER: string =
		'SELECT token, expires_after, whatsapp_server_number FROM ' +
		DATABASE_SCHEMA +
		TOKEN_TABLE +
		' where whatsapp_server_number=$1' +
		' order by expires_after desc ' +
		' limit 1';

	private readonly INSERT_TOKEN: string =
		'INSERT INTO ' +
		DATABASE_SCHEMA +
		TOKEN_TABLE +
		' (token, expires_after, whatsapp_server_number)' +
		' VALUES ($1,$2,$3)';

	constructor() {
		this.db = connection.getConnection();
	}

	public findTheLatestTokenByNumber(waServerNumber: string): Promise<any> {
		return this.getDb().any(this.SELECT_LATEST_TOKEN_BY_NUMBER, [waServerNumber]);
	}

	public insertToken(token: string, expiresAfter: string, waServerNumber: string): Promise<any> {
		return this.db.none(this.INSERT_TOKEN, [token, expiresAfter, waServerNumber]);
	}

	private getDb(): pgPromise.IDatabase<any> {
		return this.db;
	}
}

export const tokenDao: TokenDao = new TokenDao();
