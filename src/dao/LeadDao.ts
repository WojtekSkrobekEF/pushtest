import pgPromise from 'pg-promise';
import { DATABASE_SCHEMA, LEAD_TABLE } from '../configs/env_config';
import { Lead } from '../domains/Lead';
import { connection } from './Connection';

class LeadDao {
	private readonly db: pgPromise.IDatabase<Lead>;

	private readonly SELECT_LEAD: string =
		'SELECT sfid, isconverted, convertedOpportunityId FROM ' + DATABASE_SCHEMA + LEAD_TABLE + ' where sfid = $1';

	constructor() {
		this.db = connection.getConnection();
	}

	public getDb(): pgPromise.IDatabase<any> {
		return this.db;
	}

	public findLeadBySfid(sfid: string): Promise<any> {
		return this.getDb().any(this.SELECT_LEAD, [sfid]);
	}
}

export const leadDao: LeadDao = new LeadDao();
