import pgPromise from 'pg-promise';
import { File } from '../domains/File';
import { connection } from './Connection';
import { DATABASE_SCHEMA, FILES_TABLE } from '../configs/env_config';

class FileDao {
	private readonly db: pgPromise.IDatabase<File>;

	private readonly INSERT_FILE_INFO: string =
		'INSERT INTO ' +
		DATABASE_SCHEMA +
		FILES_TABLE +
		' (fileid, createddate, whatsapp_server_number, isremoved)' +
		' VALUES ($1,$2,$3,$4)';

	private readonly MARK_AS_REMOVED_BY_ID: string =
		'UPDATE ' + DATABASE_SCHEMA + FILES_TABLE + ' SET isRemoved = true' + ' WHERE fileid = $1';

	private readonly SELECT_NOT_REMOVED_FILES_BY_DATE: string =
		'SELECT * FROM ' +
		DATABASE_SCHEMA +
		FILES_TABLE +
		" where createddate <= current_date - Interval '$1 DAY' " +
		' and isremoved = false ' +
		' limit $2';

	private readonly SELECT_FILE_BY_FILEID: string =
		'SELECT * FROM ' + DATABASE_SCHEMA + FILES_TABLE + ' where fileid = $1';

	constructor() {
		this.db = connection.getConnection();
	}

	public insertFile(file: File): Promise<File> {
		return this.db.none(this.INSERT_FILE_INFO, [
			file.getFileId(),
			file.getCreatedDate(),
			file.getWaServerNumber(),
			file.getIsRemoved(),
		]);
	}

	public markRemoved(fileId: string): Promise<any> {
		return this.getDb().none(this.MARK_AS_REMOVED_BY_ID, fileId);
	}

	public fetchFilesByRemainingDays(days: number, size: number): Promise<any> {
		return this.getDb().any(this.SELECT_NOT_REMOVED_FILES_BY_DATE, [days, size]);
	}

	public fetchFileByFileId(fileId: string): Promise<any> {
		return this.getDb().any(this.SELECT_FILE_BY_FILEID, fileId);
	}

	private getDb(): pgPromise.IDatabase<any> {
		return this.db;
	}
}

export const fileDao: FileDao = new FileDao();
