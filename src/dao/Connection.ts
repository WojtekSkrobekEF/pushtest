import pgPromise from 'pg-promise';
import { DATABASE_URL } from '../configs/env_config';

export class Connection {
	private readonly connectionUrl: string;
	private readonly pgp: pgPromise.IMain;
	private readonly db: pgPromise.IDatabase<any>;

	constructor() {
		this.connectionUrl = DATABASE_URL;
		this.pgp = pgPromise();
		this.db = this.pgp(this.connectionUrl);
	}

	public getConnection(): pgPromise.IDatabase<any> {
		return this.db;
	}
}
export const connection: Connection = new Connection();
