import pgPromise from 'pg-promise';
import { DATABASE_SCHEMA, INBOUND_MESSAGE_TABLE, OUTBOUND_MESSAGE__TABLE } from '../configs/env_config';
import { IncomeMessage } from '../domains/IncomeMessage';
import { OutcomeAlphanumMessage } from '../domains/OutcomeAlphanumMessage';
import { OutcomeMessage } from '../domains/OutcomeMessage';
import { OutcomeMessageDTO } from '../dto/OutcomeMessageDTO';
import { SObjects } from '../enums/SObjects';
import { connection } from './Connection';

class MessageDAO {
	private readonly INSERT_QUERY_OUTCOME_MESSAGE: string =
		'INSERT INTO ' +
		DATABASE_SCHEMA +
		OUTBOUND_MESSAGE__TABLE +
		'(fromNumber__c, toNumber__c, message__c, parent_id__c,' +
		' sObject__c, send_by__c, opportunity__c, type__c)' +
		' VALUES($1,$2,$3,$4,$5,$6,$7,$8) RETURNING id';

	private readonly UPDATE_QUERY_OUTCOME_MESSAGE: string =
		'UPDATE ' + DATABASE_SCHEMA + OUTBOUND_MESSAGE__TABLE + ' SET status__c=$1 WHERE messageSid__c =$2';

	private readonly UPDATE_OUTCOME_TWILLIO_WA: string =
		'UPDATE ' +
		DATABASE_SCHEMA +
		OUTBOUND_MESSAGE__TABLE +
		' SET status__c=$1, error_message__c=$2 where messageSid__c=$3';

	private readonly INSERT_QUERY_INCOME_OPPORTUNITY_MESSAGE: string =
		'INSERT INTO ' +
		DATABASE_SCHEMA +
		INBOUND_MESSAGE_TABLE +
		'(fromNumber__c, toNumber__c, message__c, messageSid__c, status__c, sobject__c, opportunity__c, type__c, file_id__c, file_type__c)' +
		' VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)';

	private readonly INSERT_QUERY_INCOME_LEAD_MESSAGE: string =
		'INSERT INTO ' +
		DATABASE_SCHEMA +
		INBOUND_MESSAGE_TABLE +
		'(fromNumber__c, toNumber__c, message__c, messageSid__c, status__c, sobject__c, lead__c, type__c, file_id__c, file_type__c)' +
		' VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)';

	private readonly INSERT_QUERY_INCOME_CASE_MESSAGE: string =
		'INSERT INTO ' +
		DATABASE_SCHEMA +
		INBOUND_MESSAGE_TABLE +
		'(fromNumber__c, toNumber__c, message__c, messageSid__c, status__c, sobject__c, case__c, type__c, file_id__c, file_type__c)' +
		' VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)';

	private readonly INSERT_QUERY_INCOME_CONTACT_MESSAGE: string =
		'INSERT INTO ' +
		DATABASE_SCHEMA +
		INBOUND_MESSAGE_TABLE +
		'(fromNumber__c, toNumber__c, message__c, messageSid__c, status__c, sobject__c, contact__c, type__c, file_id__c, file_type__c)' +
		' VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)';

	private readonly INSERT_QUERY_INCOME_COMPLIANCE_ITEM_MESSAGE: string =
		'INSERT INTO ' +
		DATABASE_SCHEMA +
		INBOUND_MESSAGE_TABLE +
		'(fromNumber__c, toNumber__c, message__c, messageSid__c, status__c, sobject__c, complianceitem__c, type__c, file_id__c, file_type__c)' +
		' VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)';

	private readonly SELECT_QUERY_THE_LATEST_OUTCOME_MESSAGE: string =
		'SELECT * FROM ' +
		DATABASE_SCHEMA +
		OUTBOUND_MESSAGE__TABLE +
		' WHERE toNumber__c = $1 and type__c = $2' +
		' and isdeleted <> true' +
		' order by createddate DESC ' +
		' LIMIT 1';

	private readonly INSERT_QUERY_OUTCOME_ALPHANUMERIC_MESSAGE: string =
		'INSERT INTO ' +
		DATABASE_SCHEMA +
		OUTBOUND_MESSAGE__TABLE +
		'(toNumber__c, message__c, parent_id__c,' +
		' sObject__c, send_by__c, opportunity__c, type__c)' +
		' VALUES($1,$2,$3,$4,$5,$6,$7) RETURNING id';

	private readonly UPDATE_QUERY_OUTCOME_ALPHANUMERIC_MESSAGE: string =
		'UPDATE ' +
		DATABASE_SCHEMA +
		OUTBOUND_MESSAGE__TABLE +
		' SET status__c=$1, fromNumber__c=$2 where messageSid__c=$3';

	private readonly UPDATE_QUERY_OUTCOME_MESSAGE_BY_ID: string =
		'UPDATE ' + DATABASE_SCHEMA + OUTBOUND_MESSAGE__TABLE + ' set messagesid__c=$1 where id=$2';

	private readonly UPDATE_OUTCOME_MESSAGE_STATUS_AND_ERROR_MESSAGE_BY_ID: string =
		'UPDATE ' + DATABASE_SCHEMA + OUTBOUND_MESSAGE__TABLE + ' set status__c=$1, error_message__c=$2 where id=$3';

	private readonly db: pgPromise.IDatabase<OutcomeMessage>;

	constructor() {
		this.db = connection.getConnection();
	}

	public getDb(): pgPromise.IDatabase<any> {
		return this.db;
	}

	public saveOutComeAlphanumMessage(message: OutcomeAlphanumMessage): Promise<any> {
		return this.db.one(this.INSERT_QUERY_OUTCOME_ALPHANUMERIC_MESSAGE, [
			message.getToPhone(),
			message.getTextMessage(),
			message.getParentId(),
			message.getSObject(),
			message.getSendBy(),
			message.getOpportunity(),
			message.getType(),
		]);
	}

	public saveOutcomeMessage(message: OutcomeMessage): Promise<any> {
		return this.db.one(this.INSERT_QUERY_OUTCOME_MESSAGE, [
			message.getFromPhone(),
			message.getToPhone(),
			message.getTextMessage(),
			message.getParentId(),
			message.getSObject(),
			message.getSendBy(),
			message.getOpportunity(),
			message.getType(),
		]);
	}

	public updateMessageSidById(messageSid: string, recordId: number): Promise<any> {
		return this.db.none(this.UPDATE_QUERY_OUTCOME_MESSAGE_BY_ID, [messageSid, recordId]);
	}

	public updateMessageStatusErrorById(status: string, errorMessage: string, recordId: number): Promise<any> {
		return this.db.none(this.UPDATE_OUTCOME_MESSAGE_STATUS_AND_ERROR_MESSAGE_BY_ID, [status, errorMessage, recordId]);
	}

	public updateMessage(messageSid: string, messageStatus: string): Promise<any> {
		return this.db.none(this.UPDATE_QUERY_OUTCOME_MESSAGE, [messageStatus, messageSid]);
	}

	public updateMessageWA(messageSid: string, messageStatus: string, errorCode: string): Promise<any> {
		return this.db.none(this.UPDATE_OUTCOME_TWILLIO_WA, [messageStatus, errorCode, messageSid]);
	}

	public updateAlphanumMessage(messageSid: string, messageStatus: string, fromNumber: string): Promise<any> {
		return this.db.none(this.UPDATE_QUERY_OUTCOME_ALPHANUMERIC_MESSAGE, [messageStatus, fromNumber, messageSid]);
	}

	public returnInsertQueryBySObject(sObject: string): string {
		switch (sObject) {
			case SObjects.Case:
				return this.INSERT_QUERY_INCOME_CASE_MESSAGE;
			case SObjects.Lead:
				return this.INSERT_QUERY_INCOME_LEAD_MESSAGE;
			case SObjects.Opportunity:
				return this.INSERT_QUERY_INCOME_OPPORTUNITY_MESSAGE;
			case SObjects.Contact:
				return this.INSERT_QUERY_INCOME_CONTACT_MESSAGE;
			case SObjects.ComplianceItem:
				return this.INSERT_QUERY_INCOME_COMPLIANCE_ITEM_MESSAGE;
		}
	}

	public saveIncomeMessage(message: IncomeMessage, outMessageDTO: OutcomeMessageDTO): Promise<any> {
		return this.db.none(this.returnInsertQueryBySObject(outMessageDTO.getSObject()), [
			message.getFromPhone(),
			message.getToPhone(),
			message.getTextMessage(),
			message.getMessageSid(),
			message.getStatus(),
			outMessageDTO.getSObject(),
			outMessageDTO.getParentId(),
			message.getType(),
			message.getFileId(),
			message.getMessageContentType(),
		]);
	}

	public getTheLatestOutcomeMessage(fromNumber: string, messageType: string): Promise<any> {
		return this.db.any(this.SELECT_QUERY_THE_LATEST_OUTCOME_MESSAGE, [fromNumber, messageType]);
	}
}

export const messageDao: MessageDAO = new MessageDAO();
