import * as dotenv from 'dotenv';

let path: string;
switch (process.env.NODE_ENV) {
	case 'test': {
		path = `${__dirname}/../../.env.test`;
		break;
	}
	case 'production': {
		path = `${__dirname}/../../.env.production`;
		break;
	}
	default: {
		path = `${__dirname}/../../.env`;
	}
}

dotenv.config({ path });

export const DATABASE_SCHEMA = process.env.DATABASE_SCHEMA;
export const DATABASE_URL = process.env.DATABASE_URL;
export const SERVER_PORT = process.env.PORT || process.env.SERVER_PORT;
export const ACCOUNT_SID = process.env.ACCOUNT_SID;
export const TWILIO_AUTH_TOKEN = process.env.TWILIO_AUTH_TOKEN;
export const PRIVATE_KEY = process.env.PRIVATE_KEY;
export const SMS_STATUS_WEBHOOK_URL = process.env.SMS_STATUS_WEBHOOK_URL;
export const INBOUND_MESSAGE_TABLE = process.env.INBOUND_MESSAGE_TABLE;
export const OUTBOUND_MESSAGE__TABLE = process.env.OUTBOUND_MESSAGE__TABLE;
export const ERROR_TABLE = process.env.ERROR_TABLE;
export const LEAD_TABLE = process.env.LEAD_TABLE;
export const TOKEN_TABLE = process.env.TOKEN_TABLE;
export const FILES_TABLE = process.env.FILES_TABLE;
export const FILE_REMOVAL_SCHEDULE = process.env.FILE_REMOVAL_SCHEDULE;
export const FILE_REMOVAL_BATCH_SIZE = process.env.FILE_REMOVAL_BATCH_SIZE;
export const FILE_REMOVAL_REMAINING_DAYS = process.env.FILE_REMOVAL_REMAINING_DAYS;

// WhatsApp Credentials by country
// GB NUMBER
export const WHATSAPP_API_HOST_GB = process.env.WHATSAPP_API_HOST_GB;
export const WHATSAPP_ADMIN_LOGIN_GB = process.env.WHATSAPP_ADMIN_LOGIN_GB;
export const WHATSAPP_ADMIN_PASS_GB = process.env.WHATSAPP_ADMIN_PASS_GB;
