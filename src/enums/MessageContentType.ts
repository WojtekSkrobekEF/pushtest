export enum MessageContentType {
	IMAGE = 'image',
	VOICE = 'voice',
	VIDEO = 'video',
	DOCUMENT = 'document',
	TEXT = 'text',
}
