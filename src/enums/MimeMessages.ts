export enum MimeMessages {
	IMAGE = 'image/jpeg',
	VOICE = 'audio/mp4',
	VIDEO = 'video/mp4',
	DOCUMENT = 'document/unknown',
}
