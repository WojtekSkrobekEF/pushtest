export enum SObjects {
	Lead = 'Lead',
	Opportunity = 'Opportunity',
	Case = 'Case',
	Contact = 'Contact',
	ComplianceItem = 'ComplianceItem',
}
