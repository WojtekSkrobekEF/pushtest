export enum MessageStatus {
	QUEUED = 'Queued',
	FAILED = 'Failed',
	SENT = 'Sent',
	DELIVERED = 'Delivered',
	UNDELIVERED = 'Undelivered',
	RECEIVED = 'Received',
	COULD_NOT_BE_DELIVERED = 'Could not be delivered',
}
