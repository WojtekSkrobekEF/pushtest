import { SObjects } from '../enums/SObjects';

export class SObjectService {
	public safelyGetSObject(sObject: string): string {
		switch (sObject) {
			case SObjects.Opportunity:
				return SObjects.Opportunity;
			case SObjects.Lead:
				return SObjects.Lead;
			case SObjects.Case:
				return SObjects.Case;
			case SObjects.Contact:
				return SObjects.Contact;
			case SObjects.ComplianceItem:
				return SObjects.ComplianceItem;
			default:
				throw new Error('Please specify SalesForce Object');
		}
	}
}
export const sObjectService: SObjectService = new SObjectService();
