import Optional from 'typescript-optional';
import { messageDao } from '../dao/MessageDAO';
import { ErrorMessage } from '../domains/ErrorMessage';
import { IncomeMessage } from '../domains/IncomeMessage';
import { Lead } from '../domains/Lead';
import { OutcomeMessage } from '../domains/OutcomeMessage';
import { IncomeMessageDTO } from '../dto/IncomeMessageDTO';
import { OutcomeMessageDTO } from '../dto/OutcomeMessageDTO';
import { MessageStatus } from '../enums/MessageStatus';
import { MessageType } from '../enums/MessageType';
import { SObjects } from '../enums/SObjects';
import { errorService } from './ErrorService';
import { leadService } from './LeadService';
import { phoneNumberService } from './PhoneNumberService';
import { sObjectService } from './SObjectService';
import { twilioService } from './TwilioService';

class WhatsAppMessageService {
	public sendMessage(outWhatsAppMsgDTO: OutcomeMessageDTO): Promise<any> {
		return new Promise<any>(async (resolve, reject) => {
			const fromNumber = phoneNumberService.safelyGetTwillioPhoneNumberWhatsAppByCountry(
				outWhatsAppMsgDTO.getCountry(),
			);

			const modifiedFromNumber = phoneNumberService.createWhatsAppNumber(fromNumber);
			const validToNumber = phoneNumberService.returnSuitableNumber(outWhatsAppMsgDTO.getToNumber());
			const modifiedToNumber = phoneNumberService.createWhatsAppNumber(validToNumber);

			const message: OutcomeMessage = new OutcomeMessage(
				Optional.empty(),
				fromNumber,
				validToNumber,
				outWhatsAppMsgDTO.getMessage(),
				outWhatsAppMsgDTO.getParentId(),
				outWhatsAppMsgDTO.getSendBy(),
				sObjectService.safelyGetSObject(outWhatsAppMsgDTO.getSObject()),
				MessageType.WhatsApp,
			);

			const dbRecord = await this.saveOutcomeWhatsAppMessage(message);

			const messageInstance = twilioService.sendMessageWhatsApp(
				modifiedToNumber,
				modifiedFromNumber,
				outWhatsAppMsgDTO.getMessage(),
			);

			messageInstance
				.then(async (res) => {
					await this.updateOutcomeMessageSidById(dbRecord.id, res.sid);
					return resolve();
				})
				.catch(async (rej) => {
					console.log(rej);
					await errorService.createError(rej.status, rej.code, rej.message, message);
					await this.updateOutcomeMessageStatusErrorById(
						MessageStatus.COULD_NOT_BE_DELIVERED,
						rej.message,
						dbRecord.id,
					);
					return reject(rej);
				});
		});
	}

	public async updateStatus(messageSid: string, messageStatus: string, errorCode: string): Promise<any> {
		return this.updateWhatsAppMessageStatus(messageSid, messageStatus, errorCode);
	}

	public async fetchMessage(inWhatsAppMsgDTO: IncomeMessageDTO): Promise<any> {
		const fromPhoneNumber: string = phoneNumberService.clearWhatsAppPhoneNumber(inWhatsAppMsgDTO.getFromNumber());
		const toNumber: string = phoneNumberService.clearWhatsAppPhoneNumber(inWhatsAppMsgDTO.getToNumber());
		const outcomeMessage: OutcomeMessageDTO = await this.safelyGetLastOutComeMessage(inWhatsAppMsgDTO);
		const leadObj: Optional<Lead> = await leadService.findLeadBySfid(outcomeMessage.getParentId());

		if (leadObj.isPresent) {
			const lead: Lead = leadObj.get();

			if (lead.getIsConverted()) {
				await this.changeAndSaveIncomeMessage(outcomeMessage, inWhatsAppMsgDTO, lead, fromPhoneNumber, toNumber);
			} else {
				await this.createAndSaveIncomeMessage(outcomeMessage, inWhatsAppMsgDTO, fromPhoneNumber, toNumber);
			}
		} else {
			await this.createAndSaveIncomeMessage(outcomeMessage, inWhatsAppMsgDTO, fromPhoneNumber, toNumber);
		}
	}

	public saveMessageList(messageList: any[]): Array<Promise<any>> {
		const arrayPromise: Array<Promise<any>> = new Array(messageList.length);

		messageList.forEach((outMsg) => {
			const outWhatsAppMsgDTO = new OutcomeMessageDTO(
				outMsg.textMessage,
				outMsg.toPhone,
				outMsg.parentId,
				outMsg.sObject,
				outMsg.sendBy,
				outMsg.country,
			);

			arrayPromise.push(this.sendMessage(outWhatsAppMsgDTO));
		});

		return arrayPromise;
	}

	public async safelyGetLastOutComeMessage(dto: IncomeMessageDTO): Promise<OutcomeMessageDTO> {
		const res = await messageDao.getTheLatestOutcomeMessage(
			phoneNumberService.clearWhatsAppPhoneNumber(dto.getFromNumber()),
			MessageType.WhatsApp,
		);

		if (res.length === 1) {
			return this.mapToDTO(res);
		} else {
			const msg: string = 'Outcome message from ' + dto.getFromNumber() + ' were not found!';

			const errMessage: ErrorMessage = new ErrorMessage(
				Optional.empty(),
				'404',
				msg,
				dto.getToNumber(),
				dto.getFromNumber(),
				null,
				MessageType.WhatsApp,
			);
			await errorService.saveError(errMessage);

			throw new Error(msg);
		}
	}

	public saveIncomeWhatsAppMessage(message: IncomeMessage, outcomeMessageDTO: OutcomeMessageDTO): Promise<any> {
		return messageDao.saveIncomeMessage(message, outcomeMessageDTO);
	}

	protected mapToDTO(res: Record<string, unknown>[]): OutcomeMessageDTO {
		const outMsgObject: any = res[0];
		return new OutcomeMessageDTO(
			outMsgObject.message__c,
			outMsgObject.tonumber__c,
			outMsgObject.parent_id__c,
			outMsgObject.sobject__c,
			outMsgObject.send_by__c,
			null,
		);
	}

	protected saveOutcomeWhatsAppMessage(message: OutcomeMessage): Promise<any> {
		return messageDao.saveOutcomeMessage(message);
	}

	protected updateWhatsAppMessageStatus(messageSid: string, messageStatus: string, errorCode: string): Promise<any> {
		return messageDao.updateMessageWA(messageSid, messageStatus, errorCode);
	}

	protected updateOutcomeMessageSidById(recordId: number, messageSid: string): Promise<any> {
		return messageDao.updateMessageSidById(messageSid, recordId);
	}

	protected updateOutcomeMessageStatusErrorById(status: string, errorMessage: string, recordId: number): Promise<any> {
		return messageDao.updateMessageStatusErrorById(status, errorMessage, recordId);
	}

	protected async changeAndSaveIncomeMessage(
		outcomeMessage: OutcomeMessageDTO,
		inWhatsAppMsgDTO: IncomeMessageDTO,
		lead: Lead,
		fromPhoneNumber: string,
		toNumber: string,
	) {
		/**
		 * if found record in DB has lead.isConverted === true
		 * this means that the object was converted from Lead --> Opportunity
		 * Here we updating outcomeSMSMessageDTO( sObject and parentId fields)
		 * Creating new income message with
		 * updated details soject__c = Opportunity
		 * and opportunity__c = convertedopportunityid
		 */
		const updatedOutcomeMessage: OutcomeMessageDTO = new OutcomeMessageDTO(
			outcomeMessage.getMessage(),
			outcomeMessage.getToNumber(),
			lead.getConvertedOpportunityId(),
			SObjects.Opportunity,
			outcomeMessage.getSendBy(),
			outcomeMessage.getCountry(),
		);

		const message: IncomeMessage = new IncomeMessage(
			Optional.empty(),
			fromPhoneNumber,
			toNumber,
			inWhatsAppMsgDTO.getMessage(),
			inWhatsAppMsgDTO.getMessageSid(),
			inWhatsAppMsgDTO.getSmsStatus(),
			MessageType.WhatsApp,
			null,
			null,
		);

		await this.saveIncomeWhatsAppMessage(message, updatedOutcomeMessage);
	}

	protected async createAndSaveIncomeMessage(
		outcomeMessage: OutcomeMessageDTO,
		inWhatsAppMsgDTO: IncomeMessageDTO,
		fromPhoneNumber: string,
		toNumber: string,
	) {
		const message: IncomeMessage = new IncomeMessage(
			Optional.empty(),
			fromPhoneNumber,
			toNumber,
			inWhatsAppMsgDTO.getMessage(),
			inWhatsAppMsgDTO.getMessageSid(),
			inWhatsAppMsgDTO.getSmsStatus(),
			MessageType.WhatsApp,
			null,
			null,
		);

		await this.saveIncomeWhatsAppMessage(message, outcomeMessage);
	}
}

export const whatsAppMessageService: WhatsAppMessageService = new WhatsAppMessageService();
