import Optional from 'typescript-optional';
import { errorMessageDao } from '../dao/ErrorMessageDAO';
import { ErrorMessage } from '../domains/ErrorMessage';
import { OutcomeAlphanumMessage } from '../domains/OutcomeAlphanumMessage';
import { OutcomeMessage } from '../domains/OutcomeMessage';
import { FileError } from '../domains/FileError';

class ErrorService {
	public async createError(status: string, code: string, errMessage: string, message: OutcomeMessage): Promise<any> {
		const errorMessage: ErrorMessage = new ErrorMessage(
			Optional.empty(),
			code,
			errMessage,
			message.getToPhone(),
			message.getFromPhone(),
			message.getSObject(),
			message.getType(),
		);

		await errorMessageDao.saveErrorMessage(errorMessage);
	}

	public async createErrorAlphanumMessage(
		status: string,
		code: string,
		errMessage: string,
		message: OutcomeAlphanumMessage,
	): Promise<any> {
		const errorMessage: ErrorMessage = new ErrorMessage(
			Optional.empty(),
			code,
			errMessage,
			message.getToPhone(),
			null,
			message.getSObject(),
			message.getType(),
		);
		await errorMessageDao.saveErrorMessage(errorMessage);
	}

	public async saveError(errorMessage: ErrorMessage) {
		await errorMessageDao.saveErrorMessage(errorMessage);
	}

	public async saveErrorFile(fileError: FileError): Promise<any> {
		await errorMessageDao.saveErrorFile(fileError);
	}
}

export const errorService: ErrorService = new ErrorService();
