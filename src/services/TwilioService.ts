import Twilio from 'twilio';
import { MessageInstance } from 'twilio/lib/rest/api/v2010/account/message';
import { ACCOUNT_SID, SMS_STATUS_WEBHOOK_URL, TWILIO_AUTH_TOKEN } from '../configs/env_config';
import { OutcomeMessage } from '../domains/OutcomeMessage';

class TwilioService {
	private readonly client: any;

	constructor() {
		this.client = Twilio(ACCOUNT_SID, TWILIO_AUTH_TOKEN);
	}

	public async sendMessageSMS(message: OutcomeMessage): Promise<MessageInstance> {
		const outComingMessage = {
			body: message.getTextMessage(),
			from: message.getFromPhone(),
			statusCallback: SMS_STATUS_WEBHOOK_URL,
			to: message.getToPhone(),
		};

		return await this.client.messages.create(outComingMessage);
	}

	public async sendMessageSMSUsingService(
		toNumber: string,
		message: string,
		serviceSid: string,
	): Promise<MessageInstance> {
		const outComingMessage = {
			body: message,
			messagingServiceSid: serviceSid,
			to: toNumber,
		};

		return await this.client.messages.create(outComingMessage);
	}

	public async sendMessageWhatsApp(toNumber: string, fromNumber: string, message: string): Promise<MessageInstance> {
		const outComingMessageWhatsApp = {
			body: message,
			from: fromNumber,
			to: toNumber,
		};

		return await this.client.messages.create(outComingMessageWhatsApp);
	}
}

export const twilioService: TwilioService = new TwilioService();
