import Optional from 'typescript-optional';
import { leadDao } from '../dao/LeadDao';
import { Lead } from '../domains/Lead';
import { leadObjectMapper } from '../objectMappers/LeadObjectMapper';

class LeadService {
	public async findLeadBySfid(sfid: string): Promise<any> {
		const res: Record<string, unknown>[] = await leadDao.findLeadBySfid(sfid);

		if (res.length === 1) {
			const leadObj: any = res[0];
			const lead: Lead = leadObjectMapper.mapLead(leadObj);

			return Optional.of(lead);
		} else {
			return Optional.empty();
		}
	}
}

export const leadService: LeadService = new LeadService();
