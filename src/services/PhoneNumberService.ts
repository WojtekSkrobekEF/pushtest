import { MessageServices } from '../enums/MessageServices';
import { PhoneNumbers } from '../enums/PhoneNumbers';
import { WhatsAppNumbers } from '../enums/WhatsAppNumbers';
import { TwillioWhatsAppNumbers } from '../enums/TwillioWhatsAppNumbers';

class PhoneNumberService {
	public safelyGetMessageServiceByCountry(country: string): string {
		switch (country) {
			case 'PL': {
				return MessageServices.PL;
			}
			case 'US': {
				return MessageServices.US;
			}
			case 'SE': {
				return MessageServices.SE;
			}
			case 'HU': {
				return MessageServices.HU;
			}
			case 'AT': {
				return MessageServices.AT;
			}
			case 'FR': {
				return MessageServices.FR;
			}
			case 'DK': {
				return MessageServices.DK;
			}
			case 'NL': {
				return MessageServices.NL;
			}
			case 'GB': {
				return MessageServices.GB;
			}
			case 'CH':
				return MessageServices.CH;

			case 'IT': {
				return MessageServices.IT;
			}
			case 'DE': {
				return MessageServices.DE;
			}
			case 'ES': {
				return MessageServices.ES;
			}
			case 'ZA': {
				return MessageServices.ZA;
			}
			case 'LT': {
				return MessageServices.LT;
			}
			case 'LV': {
				return MessageServices.LV;
			}
			case 'EE': {
				return MessageServices.EE;
			}
			case 'BR': {
				return MessageServices.BR;
			}
			case 'CZ': {
				return MessageServices.CZ;
			}

			default:
				throw new Error('Please specify the country of Service you are using');
		}
	}

	public safelyGetPhoneNumberByCountry(country: string): string {
		switch (country) {
			case 'PL': {
				return PhoneNumbers.PL;
			}
			case 'SE': {
				return PhoneNumbers.SE;
			}
			case 'HU': {
				return PhoneNumbers.HU;
			}
			case 'AT': {
				return PhoneNumbers.AT;
			}
			case 'US': {
				return PhoneNumbers.US;
			}
			case 'FR': {
				return PhoneNumbers.FR;
			}
			case 'DK': {
				return PhoneNumbers.DK;
			}
			case 'NL': {
				return PhoneNumbers.NL;
			}
			case 'CZ': {
				return PhoneNumbers.CZ;
			}
			case 'GB': {
				return PhoneNumbers.GB;
			}
			case 'CH':
				return PhoneNumbers.CH;

			case 'IT': {
				return PhoneNumbers.IT;
			}
			case 'DE': {
				return PhoneNumbers.DE;
			}
			case 'ES': {
				return PhoneNumbers.ES;
			}
			case 'ZA': {
				return PhoneNumbers.ZA;
			}
			case 'LT': {
				return PhoneNumbers.LT;
			}
			case 'LV': {
				return PhoneNumbers.LV;
			}
			case 'EE': {
				return PhoneNumbers.EE;
			}
			case 'BR': {
				return PhoneNumbers.BR;
			}
			case 'CZ': {
				return PhoneNumbers.CZ;
			}

			default:
				throw new Error('Please specify the country of phone number you are using');
		}
	}

	public safelyGetPhoneNumberWhatsAppByCountry(country: string): string {
		switch (country) {
			case 'GB': {
				return WhatsAppNumbers.GB;
			}
			default:
				throw new Error('Please specify the country of WhatsApp phone number you are using');
		}
	}

	public safelyGetTwillioPhoneNumberWhatsAppByCountry(country: string): string {
		switch (country) {
			case 'US': {
				return TwillioWhatsAppNumbers.US;
			}
			default:
				throw new Error('Please specify the country of WhatsApp(Twillio) phone number you are using');
		}
	}

	public returnSuitableNumber(phoneNumber: string): string {
		if (!this.isEmpty(phoneNumber)) {
			const clearedNumber: string = phoneNumber.replace(/[^0-9]/g, '');
			return '+' + clearedNumber;
		} else {
			throw new Error('Please specify the phone phoneNumber');
		}
	}

	public createWhatsAppNumber(phoneNumber: string): string {
		return 'whatsapp:' + phoneNumber;
	}

	public clearWhatsAppPhoneNumber(phoneNumber: string): string {
		// removing word whatsapp
		return phoneNumber.substring(9, phoneNumber.length);
	}

	protected isEmpty(phoneNumber: string): boolean {
		return typeof phoneNumber === 'undefined' || phoneNumber === null || phoneNumber.trim() === '';
	}
}

export const phoneNumberService: PhoneNumberService = new PhoneNumberService();
