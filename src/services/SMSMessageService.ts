import Optional from 'typescript-optional';
import { messageDao } from '../dao/MessageDAO';
import { ErrorMessage } from '../domains/ErrorMessage';
import { IncomeMessage } from '../domains/IncomeMessage';
import { Lead } from '../domains/Lead';
import { OutcomeAlphanumMessage } from '../domains/OutcomeAlphanumMessage';
import { OutcomeMessage } from '../domains/OutcomeMessage';
import { IncomeMessageDTO } from '../dto/IncomeMessageDTO';
import { OutcomeSMSMessageDTO } from '../dto/OutcomeSMSMessageDTO';
import { MessageStatus } from '../enums/MessageStatus';
import { MessageType } from '../enums/MessageType';
import { SObjects } from '../enums/SObjects';
import { errorService } from './ErrorService';
import { leadService } from './LeadService';
import { phoneNumberService } from './PhoneNumberService';
import { sObjectService } from './SObjectService';
import { twilioService } from './TwilioService';

class SMSMessageService {
	public async performUC1SendMessage(outMessageDTO: OutcomeSMSMessageDTO): Promise<any> {
		return new Promise<any>(async (resolve, reject) => {
			if (this.isAlphaMessage(outMessageDTO.getIsAlpha())) {
				return this.sendAlphaMessage(outMessageDTO)
					.then(resolve)
					.catch((rej) => {
						return reject(rej);
					});
			}

			const fromNumber = phoneNumberService.safelyGetPhoneNumberByCountry(outMessageDTO.getCountry());

			console.log('got from number = ' + fromNumber);

			const message: OutcomeMessage = new OutcomeMessage(
				Optional.empty(),
				fromNumber,
				phoneNumberService.returnSuitableNumber(outMessageDTO.getToNumber()),
				outMessageDTO.getMessage(),
				outMessageDTO.getParentId(),
				outMessageDTO.getSendBy(),
				sObjectService.safelyGetSObject(outMessageDTO.getSObject()),
				MessageType.SMS,
			);

			console.log('Message to be saved to DB = ' + JSON.stringify(message));

			const dbRecord = await this.saveOutcomeMessage(message);

			console.log('saved DB record ' + JSON.stringify(dbRecord));

			/*
                1. Save outcome message and receive record ID
                2. Send message using Twillio
                3. Save outcome message and receive record ID

                3.1 if fulfilled --> update record by ID and SET MESSAGE SID
                3.2 if rejected --> update record by ID and set Error reason
             */

			const messageInstance = twilioService.sendMessageSMS(message);

			messageInstance
				.then(async (res) => {
					await this.updateOutcomeMessageSidById(dbRecord.id, res.sid);
					return resolve();
				})
				.catch(async (rej) => {
					console.log('error in sending message ' + rej.message);
					await errorService.createError(rej.status, rej.code, rej.message, message);
					await this.updateOutcomeMessageStatusErrorById(
						MessageStatus.COULD_NOT_BE_DELIVERED,
						rej.message,
						dbRecord.id,
					);
					return reject(rej);
				});
		});
	}

	public async performUC2UpdateMessageStatus(messageSid: string, messageStatus: string): Promise<any> {
		await this.updateMessageStatus(messageSid, messageStatus);
	}

	public async performUC4UpdateAlphanumMessageStatus(
		messageSid: string,
		messageStatus: string,
		fromNumber: string,
	): Promise<any> {
		await this.updateAlphanumMessageStatus(messageSid, messageStatus, fromNumber);
	}

	public async performUC3FetchMessage(inMessageDTO: IncomeMessageDTO): Promise<any> {
		const outcomeMessage: OutcomeSMSMessageDTO = await this.safelyGetLastOutComeMessage(inMessageDTO);
		const leadObj: Optional<Lead> = await leadService.findLeadBySfid(outcomeMessage.getParentId());

		if (leadObj.isPresent) {
			const lead: Lead = leadObj.get();

			if (lead.getIsConverted()) {
				await this.changeAndSaveIncomeMessage(inMessageDTO, outcomeMessage, lead);
			} else {
				await this.createAndSaveIncomeMessage(inMessageDTO, outcomeMessage);
			}
		} else {
			await this.createAndSaveIncomeMessage(inMessageDTO, outcomeMessage);
		}
	}

	public saveMessageList(messageList: any[]): Array<Promise<any>> {
		const arrayPromise: Array<Promise<any>> = new Array(messageList.length);

		messageList.forEach((outMsg) => {
			const outMessageDTO: OutcomeSMSMessageDTO = new OutcomeSMSMessageDTO(
				outMsg.textMessage,
				outMsg.toPhone,
				outMsg.parentId,
				outMsg.sObject,
				outMsg.sendBy,
				outMsg.country,
				outMsg.isAlpha,
			);

			arrayPromise.push(this.performUC1SendMessage(outMessageDTO));
		});

		return arrayPromise;
	}

	public async sendAlphaMessage(outMessageDTO: OutcomeSMSMessageDTO): Promise<any> {
		return new Promise<any>(async (resolve, reject) => {
			const serviceSid = phoneNumberService.safelyGetMessageServiceByCountry(outMessageDTO.getCountry());

			const message: OutcomeAlphanumMessage = new OutcomeAlphanumMessage(
				Optional.empty(),
				Optional.empty(),
				phoneNumberService.returnSuitableNumber(outMessageDTO.getToNumber()),
				outMessageDTO.getMessage(),
				outMessageDTO.getParentId(),
				outMessageDTO.getSendBy(),
				sObjectService.safelyGetSObject(outMessageDTO.getSObject()),
				MessageType.SMS,
			);

			const dbRecord: any = await this.saveOutComeAlphanumMessage(message);

			const messageInstance = twilioService.sendMessageSMSUsingService(
				outMessageDTO.getToNumber(),
				outMessageDTO.getMessage(),
				serviceSid,
			);

			messageInstance
				.then(async (res) => {
					await this.updateOutcomeMessageSidById(dbRecord.id, res.sid);
					return resolve();
				})
				.catch(async (rej) => {
					console.log(rej);
					await errorService.createErrorAlphanumMessage(rej.status, rej.code, rej.message, message);
					await this.updateOutcomeMessageStatusErrorById(
						MessageStatus.COULD_NOT_BE_DELIVERED,
						rej.message,
						dbRecord.id,
					);
					return reject(rej);
				});
		});
	}

	public async safelyGetLastOutComeMessage(inMessageDTO: IncomeMessageDTO): Promise<any> {
		const res: Record<string, unknown>[] = await messageDao.getTheLatestOutcomeMessage(
			inMessageDTO.getFromNumber(),
			MessageType.SMS,
		);

		if (res.length === 1) {
			return this.mapToSMSDTO(res);
		} else {
			const msg: string = 'Outcome message from ' + inMessageDTO.getFromNumber() + ' were not found!';

			const errMessage: ErrorMessage = new ErrorMessage(
				Optional.empty(),
				'404',
				msg,
				inMessageDTO.getToNumber(),
				inMessageDTO.getFromNumber(),
				null,
				MessageType.SMS,
			);
			await errorService.saveError(errMessage);

			throw new Error(msg);
		}
	}

	public saveIncomeMessage(message: IncomeMessage, outcomeMessageDTO: OutcomeSMSMessageDTO): Promise<any> {
		return messageDao.saveIncomeMessage(message, outcomeMessageDTO);
	}

	protected mapToSMSDTO(res: Record<string, unknown>[]): OutcomeSMSMessageDTO {
		const outMsgObject: any = res[0];
		return new OutcomeSMSMessageDTO(
			outMsgObject.message__c,
			outMsgObject.tonumber__c,
			outMsgObject.parent_id__c,
			outMsgObject.sobject__c,
			outMsgObject.send_by__c,
			null,
			null,
		);
	}

	protected updateMessageStatus(messageSid: string, messageStatus: string): Promise<any> {
		return messageDao.updateMessage(messageSid, messageStatus);
	}

	protected saveOutcomeMessage(message: OutcomeMessage): Promise<any> {
		return messageDao.saveOutcomeMessage(message);
	}

	protected updateOutcomeMessageSidById(recordId: number, messageSid: string): Promise<any> {
		return messageDao.updateMessageSidById(messageSid, recordId);
	}

	protected updateOutcomeMessageStatusErrorById(status: string, errorMessage: string, recordId: number): Promise<any> {
		return messageDao.updateMessageStatusErrorById(status, errorMessage, recordId);
	}

	protected saveOutComeAlphanumMessage(message: OutcomeAlphanumMessage): Promise<any> {
		return messageDao.saveOutComeAlphanumMessage(message);
	}

	protected updateAlphanumMessageStatus(messageSid: string, messageStatus: string, fromNumber: string): Promise<any> {
		return messageDao.updateAlphanumMessage(messageSid, messageStatus, fromNumber);
	}

	protected isAlphaMessage(isAlpha: string): boolean {
		const upperCased = isAlpha.toLocaleUpperCase();
		return upperCased === 'TRUE';
	}

	protected async changeAndSaveIncomeMessage(
		inMessage: IncomeMessageDTO,
		outMessage: OutcomeSMSMessageDTO,
		lead: Lead,
	): Promise<any> {
		/**
		 * if found record in DB has lead.isConverted === true
		 * this means that the object was converted from Lead --> Opportunity
		 * Here we updating outcomeSMSMessageDTO( sObject and parentId fields)
		 * Creating new income message with
		 * updated details soject__c = Opportunity
		 * and opportunity__c = convertedopportunityid
		 */

		const updatedOutcomeMessage: OutcomeSMSMessageDTO = new OutcomeSMSMessageDTO(
			outMessage.getMessage(),
			outMessage.getToNumber(),
			lead.getConvertedOpportunityId(),
			SObjects.Opportunity,
			outMessage.getSendBy(),
			outMessage.getCountry(),
			outMessage.getIsAlpha(),
		);

		const message: IncomeMessage = new IncomeMessage(
			Optional.empty(),
			inMessage.getFromNumber(),
			inMessage.getToNumber(),
			inMessage.getMessage(),
			inMessage.getMessageSid(),
			inMessage.getSmsStatus(),
			MessageType.SMS,
			null,
			null,
		);

		await this.saveIncomeMessage(message, updatedOutcomeMessage);
	}

	protected async createAndSaveIncomeMessage(
		inMessage: IncomeMessageDTO,
		outMessage: OutcomeSMSMessageDTO,
	): Promise<any> {
		const message: IncomeMessage = new IncomeMessage(
			Optional.empty(),
			inMessage.getFromNumber(),
			inMessage.getToNumber(),
			inMessage.getMessage(),
			inMessage.getMessageSid(),
			inMessage.getSmsStatus(),
			MessageType.SMS,
			null,
			null,
		);

		await this.saveIncomeMessage(message, outMessage);
	}
}

export const smsMessageService: SMSMessageService = new SMSMessageService();
