import Optional from 'typescript-optional';
import { tokenDao } from '../dao/TokenDao';
import { Token } from '../domains/Token';
import { DateUtils } from '../utils/DateUtils';
import { whatsAppApiTokenService } from '../whatsAppApi/WhatsAppApiTokenService';
import { tokenObjectMaper } from '../objectMappers/TokenObjectMapper';

class TokenService {
	public async tokenProcess(waServerNumber: string): Promise<Optional<Token>> {
		/**
		 * This process includes 3 steps
		 * 1. Fetch token from DB
		 *
		 * IF token is found, we need to check if it is
		 * not expired.
		 *  If it is expired, start process of fetching and saving token in DB
		 *  If not -- return this token
		 *
		 * IF token not found, start process of fetching and saving token in DB
		 */

		const optToken: Optional<Token> = await this.fetchLastTokenByNumberFromDb(waServerNumber);

		if (!optToken.isPresent) {
			return await this.fetchAndSaveWhatsAppToken(waServerNumber);
		}

		if (this.checkTokenExpiryDate(optToken) !== 1) {
			return await this.fetchAndSaveWhatsAppToken(waServerNumber);
		} else {
			return optToken;
		}
	}

	protected async fetchAndSaveWhatsAppToken(waServerNumber: string): Promise<Optional<Token>> {
		const optWhatsAppToken: Optional<Token> = await whatsAppApiTokenService.fetchAdminToken(waServerNumber);
		await this.saveToken(optWhatsAppToken);
		return optWhatsAppToken;
	}

	protected async saveToken(optFetchedToken: Optional<Token>) {
		if (optFetchedToken.isPresent) {
			const token: Token = optFetchedToken.get();
			await tokenDao.insertToken(token.getToken(), token.getExpireAfterDate().toString(), token.getWaServerNumber());
		} else {
			throw new Error('Error while saving token in DB');
		}
	}

	protected async fetchLastTokenByNumberFromDb(waServerNumber: string): Promise<Optional<Token>> {
		const res: Record<string, unknown>[] = await tokenDao.findTheLatestTokenByNumber(waServerNumber);

		if (res.length === 1) {
			const tokenObj: any = res[0];
			const token: Token = tokenObjectMaper.mapToken(tokenObj);

			return Optional.of(token);
		} else {
			return Optional.empty();
		}
	}

	protected checkTokenExpiryDate(optToken: Optional<Token>): number {
		if (optToken.isPresent) {
			const token: Token = optToken.get();
			return this.checkDate(token.getExpireAfterDate());
		} else {
			throw new Error('Expected Token is not found');
		}
	}

	protected checkDate(tokenDate: Date): number {
		const now: Date = new Date(Date.now());
		return DateUtils.compareDate(tokenDate, now);
	}
}
export const tokenService: TokenService = new TokenService();
