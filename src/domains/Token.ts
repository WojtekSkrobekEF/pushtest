export class Token {
	private readonly token: string;
	private readonly expiresAfter: Date;
	private readonly waServerNumber: string;

	constructor(token: string, expiresAfter: Date, waServerNumber: string) {
		this.token = token;
		this.expiresAfter = expiresAfter;
		this.waServerNumber = waServerNumber;

		this.validate();
	}

	public getToken(): string {
		return this.token;
	}

	public getExpireAfterDate(): Date {
		return this.expiresAfter;
	}

	public getWaServerNumber(): string {
		return this.waServerNumber;
	}

	private validate() {
		this.validateToken();
		this.validateExpireAfterDate();
		this.validateWaServerNumber();
	}

	private validateToken() {
		if (this.isEmpty(this.token)) {
			throw new Error('Token cant be empty');
		}
	}

	private validateExpireAfterDate() {
		if (this.isEmpty(this.expiresAfter.toString())) {
			throw new Error('Expire date cant be empty');
		}
	}

	private validateWaServerNumber() {
		if (this.isEmpty(this.waServerNumber)) {
			throw new Error('WhatsApp number cant be empty in the token');
		}
	}

	private isEmpty(str: string): boolean {
		return typeof str === 'undefined' || str === null || str.trim() === '';
	}
}
