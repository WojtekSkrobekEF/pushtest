import Optional from 'typescript-optional';
import { BaseMessage } from './BaseMessage';

export class IncomeMessage extends BaseMessage {
	private readonly messageSid: string;
	private readonly status: string;
	private readonly messageContentType: string;
	private readonly fileId: string;

	constructor(
		id: Optional<number>,
		fromPhone: string,
		toPhone: string,
		textMessage: string,
		messageSid: string,
		status: string,
		type: string,
		messageContentType: string,
		fileId: string,
	) {
		super(id, fromPhone, toPhone, textMessage, type);
		this.messageSid = messageSid;
		this.status = status;
		this.messageContentType = messageContentType;
		this.fileId = fileId;
		this.validateObject();
		this.validateIncomeMessage();
	}

	public getFromPhone(): string {
		return this.fromPhone;
	}

	public getToPhone(): string {
		return this.toPhone;
	}

	public getTextMessage(): string {
		return this.textMessage;
	}

	public getMessageSid(): string {
		return this.messageSid;
	}

	public getStatus(): string {
		return this.status;
	}

	public getMessageContentType(): string {
		return this.messageContentType;
	}

	public getFileId(): string {
		return this.fileId;
	}

	protected validateIncomeMessage(): void {
		this.validateMessageSidInvariants();
		this.validateStatusInvariants();
	}

	protected validateMessageSidInvariants(): void {
		if (this.isEmpty(this.messageSid)) {
			throw new Error("MessageSid can't be empty in Income message");
		}
	}

	protected validateStatusInvariants(): void {
		if (this.isEmpty(this.status)) {
			throw new Error("Status of Income message can't be empty");
		}
	}
}
