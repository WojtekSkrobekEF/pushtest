export class File {
	private readonly fileId: string;
	private readonly createdDate: Date;
	private readonly waServerNumber: string;
	private readonly isRemoved: boolean;

	constructor(fileId: string, createdDate: Date, waServerNumber: string, isRemoved: boolean) {
		this.fileId = fileId;
		this.createdDate = createdDate;
		this.waServerNumber = waServerNumber;
		this.isRemoved = isRemoved;

		this.validate();
	}

	public getFileId(): string {
		return this.fileId;
	}

	public getCreatedDate(): Date {
		return this.createdDate;
	}

	public getWaServerNumber(): string {
		return this.waServerNumber;
	}

	public getIsRemoved(): boolean {
		return this.isRemoved;
	}

	private validate() {
		this.validateFile();
		this.validateCreatedDate();
		this.validateCountry();
	}

	private validateFile() {
		if (this.isEmpty(this.fileId)) {
			throw new Error('File ID cant be empty');
		}
	}

	private validateCreatedDate() {
		if (this.isEmpty(this.createdDate.toString())) {
			throw new Error('Created date cant be empty');
		}
	}

	private validateCountry() {
		if (this.isEmpty(this.waServerNumber)) {
			throw new Error('Unknown country of whatsApp app');
		}
	}

	private isEmpty(str: string): boolean {
		return typeof str === 'undefined' || str === null || str.trim() === '';
	}
}
