import Optional from 'typescript-optional';

export class ErrorMessage {
	private readonly id: Optional<number>;
	private readonly errorCode: string;
	private readonly errorMessage: string;
	private readonly toNumber: string;
	private readonly fromNumber: string;
	private readonly sObject: string;
	private readonly type: string;

	constructor(
		id: Optional<number>,
		errorCode: string,
		errorMessage: string,
		toNumber: string,
		fromNumber: string,
		sObject: string,
		type: string,
	) {
		this.id = id;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.toNumber = toNumber;
		this.fromNumber = fromNumber;
		this.sObject = sObject;
		this.type = type;
	}

	public getId(): Optional<number> {
		return this.id;
	}

	public getErrorCode(): string {
		return this.errorCode;
	}

	public getErrorMessage(): string {
		return this.errorMessage;
	}

	public getToNumber(): string {
		return this.toNumber;
	}

	public getFromNumber(): string {
		return this.fromNumber;
	}

	public getSObject(): string {
		return this.sObject;
	}

	public getType(): string {
		return this.type;
	}
}
