import Optional from 'typescript-optional';

export class BaseMessage {
	protected readonly id: Optional<number>;
	protected readonly fromPhone: string;
	protected readonly toPhone: string;
	protected readonly textMessage: string;
	protected readonly type: string;

	constructor(id: Optional<number>, fromPhone: string, toPhone: string, textMessage: string, type: string) {
		this.id = id;
		this.fromPhone = fromPhone;
		this.toPhone = toPhone;
		this.textMessage = textMessage;
		this.type = type;
	}

	public getId(): Optional<number> {
		return this.id;
	}

	public getFromPhone(): string {
		return this.fromPhone;
	}

	public getToPhone(): string {
		return this.toPhone;
	}

	public getTextMessage(): string {
		return this.textMessage;
	}

	public getType(): string {
		return this.type;
	}

	protected validateObject(): void {
		this.validatePhoneInvariants();
		this.validateTextMessageInvariants();
	}

	protected validatePhoneInvariants(): void {
		if (this.isEmpty(this.toPhone) || this.isEmpty(this.fromPhone)) {
			throw new Error('Phone number cant be empty');
		}
	}

	protected validateTextMessageInvariants(): void {
		if (this.isEmpty(this.textMessage)) {
			throw new Error('TextMessage cant be empty');
		}
	}

	protected isEmpty(str: string): boolean {
		return typeof str === 'undefined' || str === null || str.trim() === '';
	}
}
