export class FileError {
	private readonly fileId: string;
	private readonly errorMessage: string;
	private readonly whatsAppServerNumber: string;

	constructor(fileId: string, errorMessage: string, whatsAppServerNumber: string) {
		this.fileId = fileId;
		this.errorMessage = errorMessage;
		this.whatsAppServerNumber = whatsAppServerNumber;
	}

	public getFileId(): string {
		return this.fileId;
	}

	public getErrorMessage(): string {
		return this.errorMessage;
	}

	public getWhatsAppServerNumber(): string {
		return this.whatsAppServerNumber;
	}
}
