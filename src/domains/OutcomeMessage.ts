import Optional from 'typescript-optional';
import { SObjects } from '../enums/SObjects';
import { BaseMessage } from './BaseMessage';

export class OutcomeMessage extends BaseMessage {
	private readonly parentId: string;
	private readonly sendBy: string;
	private readonly opportunity: string;
	private readonly sObject: string;

	constructor(
		id: Optional<number>,
		fromPhone: string,
		toPhone: string,
		textMessage: string,
		parentId: string,
		sendBy: string,
		sObject: string,
		type: string,
	) {
		super(id, fromPhone, toPhone, textMessage, type);

		this.sObject = sObject;
		this.parentId = parentId;
		this.sendBy = sendBy;
		this.validateObject();
		this.validateOutcomeMessage();
		this.opportunity = this.createOpportunity(sObject, parentId);
	}

	public getId(): Optional<number> {
		return this.id;
	}

	public getFromPhone(): string {
		return this.fromPhone;
	}

	public getToPhone(): string {
		return this.toPhone;
	}

	public getTextMessage(): string {
		return this.textMessage;
	}

	public getParentId(): string {
		return this.parentId;
	}

	public getSObject(): string {
		return this.sObject;
	}

	public getOpportunity(): string {
		return this.opportunity;
	}

	public getSendBy(): string {
		return this.sendBy;
	}

	public getType(): string {
		return this.type;
	}

	protected validateOutcomeMessage(): void {
		this.validateParentIdInvariants();
		this.validateSendByInvariants();
	}

	protected validateParentIdInvariants(): void {
		if (this.isEmpty(this.parentId)) {
			throw new Error('ParentId field cant be empty');
		}
	}

	protected validateSendByInvariants(): void {
		if (this.isEmpty(this.sendBy)) {
			throw new Error('SendBy field cant be empty');
		}
	}

	protected createOpportunity(sObject: string, parentId: string): string {
		if (sObject === SObjects.Opportunity) {
			return parentId;
		}
	}
}
