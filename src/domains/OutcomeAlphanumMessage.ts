import Optional from 'typescript-optional';

export class OutcomeAlphanumMessage {
	private readonly id: Optional<number>;
	private readonly fromPhone: Optional<string>;
	private readonly toPhone: string;
	private readonly textMessage: string;
	private readonly parentId: string;
	private readonly sendBy: string;
	private readonly opportunity: string;
	private readonly sObject: string;
	private readonly type: string;

	constructor(
		id: Optional<number>,
		fromPhone: Optional<string>,
		toPhone: string,
		textMessage: string,
		parentId: string,
		sendBy: string,
		sObject: string,
		type: string,
	) {
		this.id = id;
		this.fromPhone = fromPhone;
		this.toPhone = toPhone;
		this.textMessage = textMessage;
		this.sObject = sObject;
		this.parentId = parentId;
		this.sendBy = sendBy;
		this.validateOutcomeAlphanumMessage();
		this.opportunity = this.createOpportunity(sObject, parentId);
		this.type = type;
	}

	public getId(): Optional<number> {
		return this.id;
	}

	public getFromPhone(): Optional<string> {
		return this.fromPhone;
	}

	public getToPhone(): string {
		return this.toPhone;
	}

	public getTextMessage(): string {
		return this.textMessage;
	}

	public getParentId(): string {
		return this.parentId;
	}

	public getSObject(): string {
		return this.sObject;
	}

	public getOpportunity(): string {
		return this.opportunity;
	}

	public getSendBy(): string {
		return this.sendBy;
	}

	public getType(): string {
		return this.type;
	}

	protected validateOutcomeAlphanumMessage(): void {
		this.validateTextMessageInvariants();
		this.validateParentIdInvariants();
		this.validateSendByInvariants();
	}

	protected validateTextMessageInvariants(): void {
		if (this.isEmpty(this.textMessage)) {
			throw new Error('TextMessage cant be empty');
		}
	}

	protected isEmpty(str: string): boolean {
		return typeof str === 'undefined' || str === null || str.trim() === '';
	}

	protected validateParentIdInvariants(): void {
		if (this.isEmpty(this.parentId)) {
			throw new Error('ParentId field cant be empty');
		}
	}

	protected validateSendByInvariants(): void {
		if (this.isEmpty(this.sendBy)) {
			throw new Error('SendBy field cant be empty');
		}
	}

	protected createOpportunity(sObject: string, parentId: string): string {
		if (sObject === 'Opportunity') {
			return parentId;
		}
	}
}
