import Optional from 'typescript-optional';

export class Lead {
	private readonly id: Optional<number>;
	private readonly sfid: string;
	private readonly isConverted: boolean;
	private readonly convertedOpportunityId: string;

	constructor(id: Optional<number>, sfid: string, isConverted: boolean, convertedOpportunityId: string) {
		this.id = id;
		this.sfid = sfid;
		this.isConverted = isConverted;
		this.convertedOpportunityId = convertedOpportunityId;
	}

	public getId(): Optional<number> {
		return this.id;
	}

	public getIsConverted(): boolean {
		return this.isConverted;
	}

	public getConvertedOpportunityId(): string {
		return this.convertedOpportunityId;
	}

	public getSfid(): string {
		return this.sfid;
	}
}
