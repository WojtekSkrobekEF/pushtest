import { File } from '../domains/File';

class FileObjectMapper {
	public mapFile(fileObj: any): File {
		return new File(fileObj.fileid, fileObj.createddate, fileObj.whatsapp_server_number, fileObj.isremoved);
	}
}
export const fileObjectMapper: FileObjectMapper = new FileObjectMapper();
