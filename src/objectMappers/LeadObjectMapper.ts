import { Lead } from '../domains/Lead';

class LeadObjectMapper {
	public mapLead(leadObj: any): Lead {
		return new Lead(leadObj.id, leadObj.sfid, leadObj.isconverted, leadObj.convertedopportunityid);
	}
}
export const leadObjectMapper: LeadObjectMapper = new LeadObjectMapper();
