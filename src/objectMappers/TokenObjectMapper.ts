import { Token } from '../domains/Token';

class TokenObjectMapper {
	public mapToken(tokenObj: any): Token {
		return new Token(tokenObj.token, tokenObj.expires_after, tokenObj.whatsapp_server_number);
	}

	public mapAWSToken(tokenObj: any, waServerNumber: string): Token {
		return new Token(tokenObj.token, tokenObj.expires_after, waServerNumber);
	}
}
export const tokenObjectMaper: TokenObjectMapper = new TokenObjectMapper();
